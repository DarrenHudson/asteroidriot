package co.x16kgames.ar.ios;

import org.robovm.apple.foundation.NSAutoreleasePool;
import org.robovm.apple.uikit.UIApplication;
import org.robovm.apple.uikit.UIApplicationLaunchOptions;

import co.x16kgames.ar.AsteroidRiot;

import com.badlogic.gdx.backends.iosrobovm.IOSApplication;
import com.badlogic.gdx.backends.iosrobovm.IOSApplicationConfiguration;

public class IOSLauncher extends IOSApplication.Delegate {

    private IOSRequestHandler mPlatformHandler;

    @Override
    protected IOSApplication createApplication() {

        final IOSApplicationConfiguration config = new IOSApplicationConfiguration();
        config.useAccelerometer = true;
        config.useCompass = false;
        config.orientationLandscape = false;
        config.orientationPortrait = true;

        mPlatformHandler = new IOSRequestHandler();
        IOSApplication iosApplication = new IOSApplication(new AsteroidRiot(mPlatformHandler),config);
        mPlatformHandler.setIOSApplication(iosApplication);

        return iosApplication;
    }

    @Override
    public boolean didFinishLaunching(UIApplication application,UIApplicationLaunchOptions launchOptions) {
        boolean finished = super.didFinishLaunching(application,launchOptions);
        mPlatformHandler.createAds();
        mPlatformHandler.loadAds();
        return finished;
    }

    public static void main(String[] argv) {
        try (NSAutoreleasePool pool = new NSAutoreleasePool()) {
            UIApplication.main(argv,null,IOSLauncher.class);
        }
    }
}