package co.x16kgames.ar;

import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

/**
 * Static class to handle all of the assets.
 *
 * @author dhudson - 30-Aug-2014
 * @since 1.1
 */
public class Assets {

    public static float pixelDensity;

    // Names ..
    private static final int PLANET_NAME_SIZE = 5621;
    private static ArrayList<String> mPlanetNames;
    private static final String PLANET_NAME_FILE = "planets.txt";

    private static final int GALAXY_NAME_SIZE = 97;
    private static ArrayList<String> mGalaxyNames;
    private static final String GALAXY_NAME_FILE = "galaxies.txt";

    private static TextureRegion mBlockTextureRegion;

    private static TextureAtlas mTextureAtlas;

    private static BitmapFont mSmallFont;
    private static BitmapFont mMediumFont;
    private static BitmapFont mLargeFont;
    private static BitmapFont mHUDFont;

    private static BitmapFont mGCSmallFont;
    private static BitmapFont mGCMediumFont;
    private static BitmapFont mGCLargeFont;

    private static Skin mDefaultSkin;

    // Sounds ..
    private static final String LAUNCH_SOUND_FILE = "sound/launch.wav";
    private static final String EXPLOSION_SOUND_FILE = "sound/explosion.wav";
    private static final String BOUNCE_SOUND_FILE = "sound/bounce.wav";

    private static Sound mExplosionSound;
    private static Sound mLaunchSound;
    private static Sound mBounceSound;

    public static void load() throws IOException {
        // pixelDensity = calculatePixelDensity();
        loadNames();
        loadTextures();
        loadFonts();
        loadSkins();
        loadSounds();
    }

    public static void dispose() {
        mTextureAtlas.dispose();
        mSmallFont.dispose();
        mMediumFont.dispose();
        mHUDFont.dispose();
        mLargeFont.dispose();
        mGCSmallFont.dispose();
        mGCMediumFont.dispose();
        mGCLargeFont.dispose();
        mDefaultSkin.dispose();
        mExplosionSound.dispose();
        mLaunchSound.dispose();
        mBounceSound.dispose();
    }

    public static Sound getExplosionSound() {
        return mExplosionSound;
    }

    public static Sound getBounceSound() {
        return mBounceSound;
    }

    public static Sound getLaunchSound() {
        return mLaunchSound;
    }

    public static TextureRegion getBlockTextureRegion() {
        return mBlockTextureRegion;
    }

    public static TextureRegion getAsteroidTexture() {
        return mTextureAtlas.findRegion("Asteroid1");
    }

    public static Sprite getTabletSprite() {
        return mTextureAtlas.createSprite("tablet");
    }

    public static Sprite getDazHudson() {
        return mTextureAtlas.createSprite("dazhudson64");
    }

    public static Sprite getProjectileRedSprite() {
        return mTextureAtlas.createSprite("Pod-red");
    }

    public static Sprite getProjectileGreenSprite() {
        return mTextureAtlas.createSprite("Pod-green");
    }

    public static Sprite getProjectileBlackSprite() {
        return mTextureAtlas.createSprite("Pod-black");
    }

    public static Sprite getProjectileDamagedSprite() {
        return mTextureAtlas.createSprite("Pod-damage");
    }

    public static Sprite getLaunchSprite() {
        return mTextureAtlas.createSprite("launchbutton");
    }

    public static Sprite getLogoSprite() {
        return mTextureAtlas.createSprite("16K");
    }

    // It might be better just to use planet names and stick Galaxy after it..
    public static String getRandomPlanetName() {
        return mPlanetNames.get(MathUtils.random(PLANET_NAME_SIZE-1));
    }

    public static String getRandomGalaxyName() {
        return mGalaxyNames.get(MathUtils.random(GALAXY_NAME_SIZE-1));
    }

    public static Sprite getSafePlanetSprite() {
        return mTextureAtlas.createSprite("safeplanet3");
    }

    public static Sprite getSafePlanetIcon() {
        return mTextureAtlas.createSprite("safeplanet");
    }

    public static Sprite getFlameDown() {
        Sprite sprite = mTextureAtlas.createSprite("Flame");
        sprite.setRotation(180);
        return sprite;
    }

    public static Sprite getFlameLeft() {
        Sprite sprite = mTextureAtlas.createSprite("Flame");
        sprite.setRotation(90);
        return sprite;
    }

    public static Sprite getFlameRight() {
        Sprite sprite = mTextureAtlas.createSprite("Flame");
        sprite.setRotation(-90);
        return sprite;
    }

    public static Sprite getShipLightening2() {
        return mTextureAtlas.createSprite("ship2");
    }

    public static Sprite getShipLightening1() {
        return mTextureAtlas.createSprite("ship1");
    }

    public static Sprite getShipNormal() {
        return mTextureAtlas.createSprite("ship");
    }

    public static TextureRegion getMedalTextureRegion() {
        return mTextureAtlas.findRegion("Medal");
    }

    public static BitmapFont getMediumFont() {
        return mMediumFont;
    }

    public static BitmapFont getSmallFont() {
        return mSmallFont;
    }

    public static BitmapFont getLargeFont() {
        return mLargeFont;
    }

    public static BitmapFont getHUDFont() {
        return mHUDFont;
    }

    public static BitmapFont getGCSmallFont() {
        return mGCSmallFont;
    }

    public static BitmapFont getGCMediumFont() {
        return mGCMediumFont;
    }

    public static BitmapFont getGCLargeFont() {
        return mGCLargeFont;
    }

    public static Skin getDefaultSkin() {
        return mDefaultSkin;
    }

    private static void loadTextures() {

        mTextureAtlas = new TextureAtlas("TextureAtlas.txt");

        for (Texture texture:mTextureAtlas.getTextures()) {
            configureTexture(texture);
        }

        // Cache this one as its used everywhere
        mBlockTextureRegion = mTextureAtlas.findRegion("block");
    }

    private static void configureTexture(Texture texture) {
        texture.setFilter(Texture.TextureFilter.Linear,Texture.TextureFilter.Linear);
    }

    private static void loadFonts() {
        mSmallFont = loadBitmapFont("fonts/Moon16fnt.fnt");
        mMediumFont = loadBitmapFont("fonts/Moon32fnt.fnt");
        mLargeFont = loadBitmapFont("fonts/Moon64fnt.fnt");

        mHUDFont = loadBitmapFont("fonts/HUDfnt.fnt");

        mGCSmallFont = loadBitmapFont("fonts/16K16fnt.fnt");
        mGCMediumFont = loadBitmapFont("fonts/16K32fnt.fnt");
        mGCLargeFont = loadBitmapFont("fonts/16K64fnt.fnt");
    }

    private static BitmapFont loadBitmapFont(String fileName) {
        BitmapFont font = new BitmapFont(Gdx.files.internal(fileName));
        configureTexture(font.getRegion().getTexture());
        return font;
    }

    private static void loadSkins() {
        mDefaultSkin = new Skin(Gdx.files.internal("skins/uiskin.json"));
        configureTexture(mDefaultSkin.getFont("default-font").getRegion().getTexture());
    }

    private static void loadSounds() {
        mExplosionSound = Gdx.audio.newSound(
            Gdx.files.internal(EXPLOSION_SOUND_FILE));
        mBounceSound = Gdx.audio.newSound(Gdx.files.internal(BOUNCE_SOUND_FILE));
        mLaunchSound = Gdx.audio.newSound(Gdx.files.internal(LAUNCH_SOUND_FILE));
    }

    private static void loadNames() throws IOException {
        mPlanetNames = new ArrayList<String>(PLANET_NAME_SIZE);
        loadNamesFromFile(PLANET_NAME_FILE,mPlanetNames);

        mGalaxyNames = new ArrayList<String>(GALAXY_NAME_SIZE);
        loadNamesFromFile(GALAXY_NAME_FILE,mGalaxyNames);
    }

    private static void loadNamesFromFile(String fileName,ArrayList<String> list) throws IOException {
        FileHandle file = Gdx.files.internal(fileName);

        Reader reader = file.reader();

        final StringBuilder builder = new StringBuilder(20);
        int c;
        while ((c = reader.read())>=0) {
            final char ch = (char)c;
            if (ch==',') {
                list.add(builder.toString());
                builder.setLength(0);
            }
            else {
                builder.append(ch);
            }
        }

        reader.close();
    }
}
