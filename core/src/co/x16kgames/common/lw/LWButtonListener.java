package co.x16kgames.common.lw;

import com.badlogic.gdx.math.Vector2;

/**
 * Simple Button Listener.
 *
 * @author dhudson - created 9 Oct 2014
 * @since 1.1
 */
public interface LWButtonListener {

    /**
     * If there has been input which is contained within the bounds of this button, then this will be called.
     * 
     * @param location
     * @since 1.1
     */
    public void touched(Vector2 location);

}
