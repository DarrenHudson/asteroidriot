package co.x16kgames.common.explosion;

import com.badlogic.gdx.graphics.Color;

/**
 * Standard explosion.
 *
 * @author dhudson - created 9 Oct 2014
 * @since 1.1
 */
public class Collision extends Explosion {

    public Collision() {
        super(150,Color.RED);
    }

}
