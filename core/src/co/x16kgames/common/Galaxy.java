package co.x16kgames.common;

import co.x16kgames.ar.Assets;
import co.x16kgames.common.planets.Asteroid;
import co.x16kgames.common.planets.Federation;
import co.x16kgames.common.planets.SafePlanet;
import co.x16kgames.common.projectile.Projectile;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Array;

/**
 * Galaxy class.
 *
 * Galaxy's contain a safe planet. Planets Asteroids.
 *
 * @author dhudson - 06-Sep-2014
 * @since 1.1
 */
public class Galaxy {

    // Virtual resolution
    public static float mHeight;
    public static float mWidth;

    /*
     * If the distance from the project to the body is greater than this distance, then no action will be taken,
     * reducing this distance make game play harder.
     */
    public static float mGravityDistance;

    public static Rectangle mGalaxyBounds;

    public static StarField mStarField;

    private static SafePlanet mSafePlanet;

    private static Array<Asteroid> mAsteroids;

    private static Federation mFederation;

    // Galaxy Name
    private static String mName;

    public Galaxy() {
        // Watch the order here.. Bounds must come first.
        // Take out the platform as we do not want bodies / stars on this
        VirtualViewport viewport = new VirtualViewport();
        mHeight = viewport.getHeight();
        mWidth = viewport.getWidth();

        mGalaxyBounds = new Rectangle(0,Platform.PLATFORM_HEIGHT,mWidth,mHeight-Platform.PLATFORM_HEIGHT);

        mStarField = new StarField();
        mSafePlanet = new SafePlanet();
        mAsteroids = new Array<Asteroid>(5);
        mFederation = new Federation();
        mFederation.setBounds(0,Platform.PLATFORM_HEIGHT,mWidth,mHeight-Platform.PLATFORM_HEIGHT);
    }

    public static void init() {
        mStarField.init();

        mGravityDistance = 30;

        mName = Assets.getRandomGalaxyName();
    }

    public static void populateGalaxy() {
        mFederation.clear();
        mStarField.init();

        mSafePlanet.init();
        mFederation.checkAndAddBody(mSafePlanet);

        // Increase the number of Asteroid each level
        int asteroids = 10+(GameStatus.mLevel*3);
        for (int i = 0;i<asteroids;i++) {
            mFederation.checkAndAddBody(getAsteroid(i));
        }
    }

    private static Asteroid getAsteroid(int index) {
        Asteroid asteroid;

        if (index<mAsteroids.size) {
            asteroid = mAsteroids.get(index);
        }
        else {
            asteroid = new Asteroid();
            mAsteroids.add(asteroid);
        }

        asteroid.init();
        return asteroid;
    }

    public static void levelUp() {
        // Each level that passes, gravity takes more effect.
        mGravityDistance++;

        mName = Assets.getRandomGalaxyName();

        populateGalaxy();
    }

    public static void render(Graphics graphics) {
        mStarField.render(graphics);
        mFederation.render(graphics);
    }

    public static void update(float delta) {
        mStarField.update(delta);
        mFederation.update(delta);
    }

    public static void interact(Projectile projectile) {
        projectile.beginInteract();
        mFederation.interact(projectile);
    }

    public static String getName() {
        return mName;
    }

    public static SafePlanet getSafePlanet() {
        return mSafePlanet;
    }
}
