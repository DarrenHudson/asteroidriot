package co.x16kgames.ar.android;

import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import co.x16kgames.ar.AsteroidRiot;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;

public class AndroidLauncher extends AndroidApplication {

    private AndroidRequestHandler mPlatformHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Libgdx config
        AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
        config.useAccelerometer = true;
        config.useCompass = false;

        // Platform independent handler
        mPlatformHandler = new AndroidRequestHandler(this);
        mPlatformHandler.loadAds();

        // Create the game
        AsteroidRiot game = new AsteroidRiot(mPlatformHandler);

        // Do the stuff that initialize() would do for you
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);

        RelativeLayout.LayoutParams adParams =
            new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        adParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        adParams.addRule(RelativeLayout.CENTER_IN_PARENT);
        RelativeLayout layout = new RelativeLayout(this);
        layout.addView(initializeForView(game,config));
        layout.addView(mPlatformHandler.getBannerAddView(),adParams);

        setContentView(layout);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPlatformHandler.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPlatformHandler.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mPlatformHandler.onPause();
    }

}
