package co.x16kgames.common.explosion;

import co.x16kgames.common.Graphics;

import com.badlogic.gdx.utils.Array;

/**
 * Display many explosions at once.
 *
 * @author dhudson - created 9 Oct 2014
 * @since 1.1
 */
public class Fireworks {

    private final Array<Firework> mFireworks;

    public Fireworks() {
        mFireworks = new Array<Firework>(4);
    }

    public void update(float delta) {
        for (Firework firework:mFireworks) {
            firework.update(delta);
        }
    }

    public void render(Graphics graphics) {
        for (Firework firework:mFireworks) {
            firework.render(graphics);
        }
    }

    public void init() {
        for (Firework firework:mFireworks) {
            firework.init();
        }
    }

    public void addFirework(Firework firework) {
        mFireworks.add(firework);
    }

    public void setDead() {
        for (Firework firework:mFireworks) {
            firework.setDead();
        }
    }
}
