package co.x16kgames.ar.screens;

import co.x16kgames.ar.Assets;
import co.x16kgames.common.AbstractGame;
import co.x16kgames.common.GamePreferences;
import co.x16kgames.common.GameStatus;
import co.x16kgames.common.Graphics;
import co.x16kgames.common.Platform;
import co.x16kgames.common.Utils;
import co.x16kgames.common.lw.LWSpriteButton;
import co.x16kgames.common.panels.AbstractPanel;
import co.x16kgames.common.panels.CrashedPanel;
import co.x16kgames.common.panels.CrashedWithPlanet;
import co.x16kgames.common.panels.CrashedWithPlatform;
import co.x16kgames.common.panels.LevelPanel;
import co.x16kgames.common.projectile.Projectile;
import co.x16kgames.common.screens.AbstractGameScreen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;

/**
 * Heads up Display.
 *
 * Display game info.
 *
 * @author dhudson - 07-Sep-2014
 * @since 1.1
 */
public class HUDScreen extends AbstractGameScreen {

    // Need a new camera, as this one doesn't pan.
    private final Camera mCamera;
    private final Sprite mProjectileSprite;
    private final Platform mPlatform;
    private final Projectile mProjectile;
    private final BitmapFont mFont;

    private final Stage mStage;

    private final LWSpriteButton mFire;
    private AbstractPanel mStatusPanel;
    private final LevelPanel mLevelPanel;
    private final CrashedPanel mCrashedPanel;
    private final CrashedWithPlanet mCrashedWithPlanetPanel;
    private final CrashedWithPlatform mCashedWithPlatformPanel;

    // Performance boost, only have one of these
    private final StringBuilder mTmpBuilder;

    public HUDScreen(AbstractGame game, GameScreen gameScreen) {
        super(game);

        mPlatform = gameScreen.mPlatform;
        mProjectile = mPlatform.getProjectile();
        mCamera = getViewport().getCamera();

        mTmpBuilder = new StringBuilder(200);

        mStage = new Stage(getViewport());
        mStage.clear();

        Sprite launchPod = new Sprite(Assets.getLaunchSprite());
        // Maintain aspect ratio
        launchPod.setSize(64, 54);
        launchPod.setOriginCenter();

        mFire = new LWSpriteButton(launchPod);
        mFire.setWidth(64);
        mFire.setHeight(64);
        mFire.backgroundRequired(false);
        mFire.setAlpha(1f);

        mProjectileSprite = new Sprite(Assets.getProjectileRedSprite());
        mProjectileSprite.setSize(16, 16);

        mFont = Assets.getHUDFont();

        // Create the status panels
        mLevelPanel = new LevelPanel(mCamera);
        mCrashedPanel = new CrashedPanel(mCamera);
        mCrashedWithPlanetPanel = new CrashedWithPlanet(mCamera);
        mCashedWithPlatformPanel = new CrashedWithPlatform(mCamera);
        mStatusPanel = mLevelPanel;
    }

    public Stage getStage() {
        return mStage;
    }

    public void update(float delta) {
        mStage.act(delta);
        if (GameStatus.isPaused()) {
            mFire.setVisible(false);
        } else if (mProjectile.isDocked() || mProjectile.isInOrbit()) {
            mFire.setVisible(true);
        } else {
            mFire.setVisible(false);
        }
    }

    public void drawStage(float delta) {
        mStage.draw();
    }

    public void render(Graphics graphics) {

        // Set the camera
        graphics.getBatch().setProjectionMatrix(mCamera.combined);

        final float offset = 20;

        for (int i = 0; i < GameStatus.getRemainingProjectiles(); i++) {
            graphics.drawSprite(mProjectileSprite, offset * i, getViewportHeight() - 20);
        }

        switch (mProjectile.getState()) {
            case BOOST:
            case INFLIGHT:
                renderInFlightDetails(graphics);
                break;

            case INORBIT:
                renderInOrbitDetails(graphics);
                break;

            case LANDING:
                renderLandingDetails(graphics);
                break;

            default:
                break;

        }

        mFire.render(graphics);

        if (Utils.isDebugging()) {
            renderDebugInfo(graphics);
        }
    }

    private void renderInFlightDetails(Graphics graphics) {
        StringBuilder builder = getStringBuilder();

        builder.append("Distance : ");
        builder.append(Utils.formatFloat(mProjectile.getDistance()));
        builder.append("\n");
        builder.append("Speed : ");
        builder.append(Utils.formatFloat(Utils.abs(mProjectile.getYVelocity())));
        builder.append("\n");
        builder.append("G : ");
        builder.append(Utils.formatFloat(mProjectile.getG()));

        addDamageAndFuel(builder);

        mFont.drawMultiLine(graphics.getBatch(), builder.toString(), 10, 120);
    }

    private void addDamageAndFuel(StringBuilder builder) {
        builder.append("\n");
        builder.append("Damage : ");
        builder.append(Utils.formatFloat(mProjectile.getDamage()));
        builder.append(" %");

        builder.append("\n");
        builder.append("Fuel remaining : ");
        builder.append(Utils.formatFloat(mProjectile.getFuelRemainingAsPercent()));
        builder.append(" %");
    }

    /**
     * Reuse a string builder to stop GC.
     *
     * @return a reset string builder
     */
    private StringBuilder getStringBuilder() {
        mTmpBuilder.setLength(0);
        return mTmpBuilder;
    }

    private void renderInOrbitDetails(Graphics graphics) {
        StringBuilder builder = getStringBuilder();
        builder.append("Orbiting : ");
        builder.append(mProjectile.getAscendingNodeName());

        addDamageAndFuel(builder);

        mFont.drawMultiLine(graphics.getBatch(), builder.toString(), 10, 120);
    }

    private void renderLandingDetails(Graphics graphics) {
        StringBuilder builder = getStringBuilder();
        builder.append("Landing : ");
        builder.append(mProjectile.getAscendingNodeName());

        addDamageAndFuel(builder);

        mFont.drawMultiLine(graphics.getBatch(), builder.toString(), 10, 120);
    }

    private void renderDebugInfo(Graphics graphics) {
        float x = mCamera.viewportWidth - 60;
        float y = 20;
        int fps = Gdx.graphics.getFramesPerSecond();

        if (fps >= 45) {
            // 45 or more FPS show up in green
            mFont.setColor(0, 1, 0, 1);
        } else if (fps >= 30) {
            // 30 or more FPS show up in yellow
            mFont.setColor(1, 1, 0, 1);
        } else {
            // less than 30 FPS show up in red
            mFont.setColor(1, 0, 0, 1);
        }
        mFont.draw(graphics.getBatch(), "FPS: " + fps, x, y);
        mFont.setColor(1, 1, 1, 1); // white

    }

    public void displayStatusWindow() {
        if (mStatusPanel.isVisible()) {
            return;
        }

        // Swap to the correct panel
        if (GameStatus.isProjectileCrashed()) {
            if (GameStatus.crashedIntoPlatform()) {
                mStatusPanel = mCashedWithPlatformPanel;
            } else if (GameStatus.getCrashedInto() == null) {
                mStatusPanel = mCrashedPanel;
            } else {
                mStatusPanel = mCrashedWithPlanetPanel;
            }
        } else {
            mStatusPanel = mLevelPanel;
        }

        // Build the window and set visible
        mStatusPanel.populate();
        mStatusPanel.setVisible(true);
        mStage.addActor(mStatusPanel.getRoot());
    }

    public void hideStatusWindow() {
        mStatusPanel.setVisible(false);
        mStage.getActors().removeValue(mStatusPanel.getRoot(), true);
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);

        if (GamePreferences.isLeftHanded) {
            mFire.setLocation(6, 200);
        } else {
            mFire.setLocation(mCamera.viewportWidth - 64 - 6, 200);
        }
    }

    @Override
    public void dispose() {
        mStage.dispose();
    }

    @Override
    public void render(Graphics graphics, float delta) {
    }

    @Override
    public boolean tap(Vector2 location) {
        if (mFire.contains(location)) {
            mPlatform.fire();
        }

        return true;
    }

    @Override
    public void create() {
    }

}
