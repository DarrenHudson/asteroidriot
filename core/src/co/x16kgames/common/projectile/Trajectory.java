package co.x16kgames.common.projectile;

import co.x16kgames.ar.Assets;
import co.x16kgames.common.Graphics;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;

/**
 * Represents the flow of travel (tracer)
 *
 * Have this as a ring buffer to stop newing of Vector2's.
 * 
 * @author dhudson - created 27 Sep 2014
 * @since 1.1
 */
public class Trajectory {

    private static final short TRACER_SIZE = 20;

    private short mHead;
    private short mTail;
    private final Vector2[] mBuffer;

    private final Projectile mProjectile;

    private int mTrajectoryCount;
    private float mDistance;
    private final Vector2 mLastPosition;

    Trajectory(Projectile projectile) {
        mProjectile = projectile;
        mLastPosition = new Vector2();
        mBuffer = new Vector2[TRACER_SIZE];
        for (short i = 0;i<TRACER_SIZE;i++) {
            mBuffer[i] = new Vector2();
        }
    }

    void init() {
        mHead = 0;
        mTail = 0;
        mTrajectoryCount = 0;
        mDistance = 0;
    }

    void render(Graphics graphics) {
        graphics.setColor(Color.DARK_GRAY);

        short index = mHead;

        while (index!=mTail) {
            graphics.getBatch().draw(Assets.getBlockTextureRegion(),mBuffer[index].x,mBuffer[index].y,2,2);
            index++;
            if (index==TRACER_SIZE) {
                index = 0;
            }
        }
        graphics.setColor(Color.WHITE);
    }

    boolean isEmpty() {
        return mHead==mTail;
    }

    private void add(float x,float y) {
        mBuffer[mTail].set(x,y);
        mTail++;
        if (mTail==TRACER_SIZE) {
            mTail = 0;
        }

        if (mTail==mHead) {
            mHead++;
            if (mHead==TRACER_SIZE) {
                mHead = 0;
            }
        }
    }

    private void remove() {
        if (mHead!=mTail) {
            mHead++;
            if (mHead==TRACER_SIZE) {
                mHead = 0;
            }
        }
    }

    void update(float delta) {
        switch (mProjectile.getState()) {
            case LANDING:
            case INORBIT:
                remove();
                break;

            case INFLIGHT:
            case BOOST:
                mTrajectoryCount++;
                if (mTrajectoryCount!=1) {
                    mDistance += mProjectile.mLocation.dst(mLastPosition);
                }
                if (mTrajectoryCount%5==0) {
                    add(mProjectile.mLocation.x+(Projectile.HALF_PROJECTILE_SIZE)-1,(mProjectile.mLocation.y+Projectile.HALF_PROJECTILE_SIZE-1));
                }

                mLastPosition.set(mProjectile.mLocation);
                break;

            default:
                break;
        }
    }

    int getTrajectoryCount() {
        return mTrajectoryCount;
    }

    float getDistance() {
        return mDistance;
    }
}
