package co.x16kgames.common.explosion;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import co.x16kgames.common.Graphics;

/**
 * Class to hold particles and manage the explosion.
 *
 * @author dhudson - 20-Sep-2014
 * @since 1.1
 */
public class Explosion {

    private static final int MAX_PARTICLES = 20;

    private final Array<Particle> mParticles;
    private boolean isDead;
    private int mLifeTime;

    private final int mMaxLifetime;

    public Explosion(int maxLifeTime,Color color) {
        mMaxLifetime = maxLifeTime;
        mParticles = new Array<Particle>(MAX_PARTICLES);
        for (int i = 0;i<MAX_PARTICLES;i++) {
            mParticles.add(new Particle(color));
        }
        isDead = true;
    }

    public void init(Vector2 position) {
        for (Particle particle:mParticles) {
            particle.init(position,mMaxLifetime);
        }
        isDead = false;
        mLifeTime = mMaxLifetime;
    }

    public void update(float delta) {
        if (!isDead) {
            if (--mLifeTime<=0) {
                isDead = true;
            }
            else {
                for (Particle particle:mParticles) {
                    particle.update(delta);
                }
            }
        }
    }

    public void render(Graphics graphics) {
        if (!isDead) {
            for (Particle particle:mParticles) {
                particle.render(graphics);
            }
            // Reset the colour after drawing the particles.
            graphics.getBatch().setColor(Color.WHITE);
        }
    }

    public void setDead() {
        isDead = true;
    }

    public boolean isDead() {
        return isDead;
    }
}
