package co.x16kgames.common;

import com.badlogic.gdx.math.MathUtils;

/**
 * Class to represent an Angle from 0 - 360.
 *
 * @author dhudson - created 11 Mar 2014
 * @since 1.0
 */
public class Angle {

    private float theAngle;

    /**
     * Constructor.
     *
     * Creates an Angle of Zero.
     *
     * @since 1.0
     */
    public Angle() {
        theAngle = 0;
    }

    /**
     * Constructor.
     *
     * @param angle in degrees
     * @since 1.0
     */
    public Angle(float angle) {
        theAngle = angle;
    }

    /**
     * Return the angle in degrees.
     *
     * @return the angle in degrees
     * @since 1.0
     */
    public float getAngle() {
        return theAngle;
    }

    /**
     * Increment the amount.
     *
     * Note .. can be negative ...
     *
     * @param amount to change the angle by
     * @since 1.0
     */
    public void increment(float amount) {
        theAngle += amount;

        if (theAngle > 360F) {
            theAngle = theAngle - 360F;
        }

        if (theAngle < 0) {
            theAngle = 360F - theAngle;
        }
    }

    /**
     * Set the angle.
     *
     * @param angle
     * @since 1.0
     */
    public void setAngle(float angle) {
        theAngle = angle;
        // Make sure that it is in range
        increment(0);
    }

    /**
     * Return the angle in radians.
     *
     * @return the angle in radians
     * @since 1.0
     */
    public float toRadians() {
        return MathUtils.degreesToRadians * theAngle;
    }

    public static float toRadians(float degrees) {
        return MathUtils.degreesToRadians * degrees;
    }

    public static float toDegrees(float radians) {
        return MathUtils.radiansToDegrees * radians;
    }

    @Override
    public String toString() {
        return "Angle{" + theAngle + '}';
    }

}
