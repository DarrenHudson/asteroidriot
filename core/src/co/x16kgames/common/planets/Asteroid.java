package co.x16kgames.common.planets;

import co.x16kgames.common.Angle;
import co.x16kgames.ar.Assets;
import co.x16kgames.common.Galaxy;
import co.x16kgames.common.GamePreferences;
import co.x16kgames.common.GameStatus;
import co.x16kgames.common.Graphics;
import co.x16kgames.common.SoundManager;
import co.x16kgames.common.Utils;
import co.x16kgames.common.explosion.Collision;
import co.x16kgames.common.explosion.Explosion;
import co.x16kgames.common.medals.MedalManager;
import co.x16kgames.common.projectile.Projectile;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;

/**
 * Simple Asteroid class.
 *
 * @author dhudson - 30-Aug-2014
 * @since 1.0
 */
public final class Asteroid extends RotatingBody {

    private static final int MEDAL_COUNT = 100;

    private float mDamage;
    private Explosion mExplosion;

    public Asteroid() {
        super(new Sprite(Assets.getAsteroidTexture()));
    }

    @Override
    public void init() {
        super.init();

        // Link size (although not mass) and gravity
        radius = MathUtils.random(10f,15f);
        setMass(radius+20);

        configureSprite();
        setLocation(Utils.getRandomPoint(Galaxy.mGalaxyBounds));

        mDamage = 0;
    }

    @Override
    public void render(Graphics graphics) {
        if (mExplosion!=null&&!mExplosion.isDead()) {
            mExplosion.render(graphics);
        }
        else {
            super.render(graphics);
        }
    }

    @Override
    public void update(float delta) {
        if (!isDead()) {
            if (mExplosion!=null&&!mExplosion.isDead()) {
                mExplosion.update(delta);
            }
            else {
                super.update(delta);
            }
        }
    }

    @Override
    public boolean isDead() {
        return (mDamage>100&&mExplosion.isDead());
    }

    @Override
    public String getType() {
        return "Asteroid";
    }

    @Override
    public void projectileHit(Projectile projectile) {

        // Exploding, so shoudn't do anything.
        if (mDamage>100) {
            return;
        }

        projectile.inflictDamage(radius*2);

        if (projectile.isBroken()) {
            GameStatus.setBodyCrashedInto(this);
            projectile.setDead(true);
            return;
        }

        float radians = Angle.toRadians(calculateAngle(projectile.getCentreLocation()));

        rotate(projectile,radians);

        // Three hits is enough
        mDamage += 33.4;
        if (mDamage>100) {
            // Don't let it have any more effect
            setMass(0f);

            if (GamePreferences.incAsteroidsDestroyed()==MEDAL_COUNT) {
                GameStatus.awardMedal(MedalManager.ASTEROID_SMASHER);
            }

            if (mExplosion==null) {
                mExplosion = new Collision();
            }

            SoundManager.playExplosion();
            mExplosion.init(projectile.getCentreLocation());
        }

        SoundManager.playBounce();
    }

    /**
     * Bounce the projectile.
     * 
     * @param projectile
     * @param theta
     * @since 1.1
     */
    private void rotate(Projectile projectile,float theta) {
        float sinTheta = MathUtils.sin(theta);
        float cosTheta = MathUtils.cos(theta);
        projectile.setVelocity(MathUtils.clamp(x*cosTheta+y*sinTheta,-100,100),-projectile.getYVelocity());
    }
}
