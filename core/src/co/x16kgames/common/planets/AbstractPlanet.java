package co.x16kgames.common.planets;

import co.x16kgames.common.Galaxy;
import co.x16kgames.common.Graphics;
import co.x16kgames.common.projectile.Projectile;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Array;

/**
 * Class that represents a planet.
 *
 * In our version a planet is a body which can have orbiting bodies.
 * 
 * A planet can be orbited.
 *
 * @author dhudson - created 14 Feb 2014
 * @since 1.0
 */
public abstract class AbstractPlanet extends Body {

    private static final float MASS_MIN = 35f;
    private static final float MASS_MAX = 50f;

    private final Array<OrbitingBody> theOrbitingBodies;

    static final int ORBIT_DISTANCE = 10;

    /**
     * Constructor.
     *
     * @since 1.0
     */
    public AbstractPlanet(Sprite sprite) {
        super(sprite);
        theOrbitingBodies = new Array<OrbitingBody>(3);
    }

    @Override
    public void init() {
        setBodyName();

        theOrbitingBodies.clear();

        // This sets the size in pixels of the plant
        radius = MathUtils.random(20f,40f);

        configureSprite();

        // Set up the mass for gravitational effect
        setMass(MathUtils.random(MASS_MIN,MASS_MAX));

        changeLocation(Galaxy.mGalaxyBounds);
    }

    @Override
    public void render(Graphics graphics) {

        // If the moon is behind the planet, draw it
        for (OrbitingBody moon:getOrbitingBodies()) {
            moon.preParentRender(graphics);
        }

        drawSprite(graphics);

        // If the moon is infront of the planet, draw it
        for (OrbitingBody moon:getOrbitingBodies()) {
            moon.postParentRender(graphics);
        }

    }

    @Override
    public Rectangle getBounds() {

        mTmpRect.x = x;
        mTmpRect.y = y;

        float maxOrbit = 0;
        float maxRadius = 0;

        for (OrbitingBody moon:getOrbitingBodies()) {
            if (moon.getOrbit()>maxOrbit) {
                maxOrbit = moon.getOrbit();
            }
            if (moon.radius>maxRadius) {
                maxRadius = moon.radius;
            }
        }

        if (maxOrbit==0) {
            // Has no orbiting bodies
            mTmpRect.width = radius*2;
            mTmpRect.height = radius*2;
            return mTmpRect;
        }

        // We have orbits
        mTmpRect.x = getCentreLocation().x-(maxOrbit/2);
        mTmpRect.y = getCentreLocation().y-(maxOrbit/2);

        mTmpRect.width = maxOrbit+maxRadius;
        mTmpRect.height = maxOrbit+maxRadius;

        return mTmpRect;
    }

    @Override
    public String getType() {
        return "Planet";
    }

    /**
     * Game tick update
     *
     * @param delta since last update
     * @since 1.1
     */
    @Override
    public void update(float delta) {
        for (OrbitingBody moon:getOrbitingBodies()) {
            moon.update(delta);
        }
    }

    @Override
    public void interact(Projectile projectile) {

        if (projectile.isDead()) {
            return;
        }

        // From the centre of the planet to the projectile
        float distance = projectile.getCentreLocation().dst(getCentreLocation());
        // minus the radius of the planet ...
        distance -= (radius);

        // Projectile has hit the planet
        if (distance<1) {
            projectileHit(projectile);
            return;
        }

        if (!projectile.isInOrbit()) {
            // Check to see if we are going to be placed in orbit.
            placeInOrbit(distance,projectile);
            calculateGravityEffect(distance,projectile);
        }

        for (OrbitingBody moon:getOrbitingBodies()) {
            moon.interact(projectile);
        }
    }

    /**
     * Return a list of orbiting bodies.
     *
     * @return a list of orbiting bodies, which may be empty.
     * @since 1.0
     */
    public Array<OrbitingBody> getOrbitingBodies() {
        return theOrbitingBodies;
    }

    /**
     * Return the number of moons.
     * 
     * @return
     * @since 1.1
     */
    public int getNumberOfOrbitingBodies() {
        return theOrbitingBodies.size;
    }

    /**
     * Add an orbiting body
     *
     * @param orbitingBody to add.
     * @since 1.0
     */
    public void addOrbitingBody(OrbitingBody orbitingBody) {
        theOrbitingBodies.add(orbitingBody);
    }

    public abstract void placeInOrbit(float distance,Projectile projectile);

    public abstract boolean canHaveMoons();

}
