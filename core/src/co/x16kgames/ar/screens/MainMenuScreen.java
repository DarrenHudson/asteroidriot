package co.x16kgames.ar.screens;

import co.x16kgames.ar.Assets;
import co.x16kgames.common.AbstractGame;
import co.x16kgames.common.Galaxy;
import co.x16kgames.common.GameStatus;
import co.x16kgames.common.Graphics;
import co.x16kgames.common.StarField;
import co.x16kgames.common.lw.LWButtonGroup;
import co.x16kgames.common.lw.LWButtonListener;
import co.x16kgames.common.lw.LWTextButton;
import co.x16kgames.common.medals.Medal;
import co.x16kgames.common.medals.MedalManager;
import co.x16kgames.common.planets.Asteroid;
import co.x16kgames.common.planets.Federation;
import co.x16kgames.common.planets.SafePlanet;
import co.x16kgames.common.screens.AbstractGameScreen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * Main menu screen.
 *
 * @author dhudson - 26-Aug-2014
 * @since 1.1
 */
public class MainMenuScreen extends AbstractGameScreen {

    private final SafePlanet mSafePlanet;

    private final StarField mStarField;

    private LWButtonGroup mButtonGroup;

    private final Federation mFederation;

    public MainMenuScreen(final AbstractGame game) {
        super(game);

        mStarField = Galaxy.mStarField;

        mSafePlanet = new SafePlanet();
        mSafePlanet.init();

        mFederation = new Federation();
        createButtons();
    }

    @Override
    public void create() {
        Vector2 location = new Vector2(getHalfViewportWidth()-70,getViewportHeight()-160);

        mSafePlanet.calculateImageShape(60,location);

        mFederation.checkAndAddBody(mSafePlanet);

        Rectangle rect = new Rectangle();

        rect.x = 0;
        rect.y = getViewportHeight()-200;
        rect.width = getViewportWidth();
        rect.height = 200;
        mFederation.setBounds(rect);

        Asteroid asteroid;
        for (int i = 0;i<5;i++) {
            asteroid = new Asteroid();
            asteroid.init();
            asteroid.changeLocation(rect);
            mFederation.checkAndAddBody(asteroid);
        }

    }

    private void createButtons() {
        mButtonGroup = new LWButtonGroup();

        LWTextButton newGame = new LWTextButton("New Game");
        mButtonGroup.add(newGame);
        newGame.setLocation(220,200).setHeight(40).setWidth(80);
        newGame.setListener(new LWButtonListener() {
            @Override
            public void touched(Vector2 location) {
                GameStatus.resetLevel();
                getScreenManager().showGameScreen();
            }
        });

        LWTextButton creditsButton = new LWTextButton("Credits");
        mButtonGroup.add(creditsButton);
        creditsButton.setLocation(100,200).setHeight(40).setWidth(80);
        creditsButton.setListener(new LWButtonListener() {
            @Override
            public void touched(Vector2 location) {
                getScreenManager().showCreditsScreen();
            }
        });

        LWTextButton instructButton = new LWTextButton("Instructions");
        mButtonGroup.add(instructButton);
        instructButton.setLocation(340,200).setHeight(40).setWidth(80);
        instructButton.setListener(new LWButtonListener() {
            @Override
            public void touched(Vector2 location) {
                getScreenManager().showInstructionScreen();
            }
        });

    }

    @Override
    public void render(Graphics graphics,float delta) {

        // Paint the star field
        mStarField.render(graphics);

        mFederation.render(graphics);

        Assets.getLargeFont().drawMultiLine(graphics.getBatch(),"Asteroid\nRiot!",
            getHalfViewportWidth(),
            getHalfViewportHeight()+200,
            10,BitmapFont.HAlignment.CENTER);

        Assets.getMediumFont().drawMultiLine(graphics.getBatch(),"Rank",100,400,10,BitmapFont.HAlignment.LEFT);

        float y = 340;

        Medal rank = MedalManager.getHighestRank();
        rank.setPosition(110,y);
        rank.render(graphics);

        Assets.getMediumFont().drawMultiLine(graphics.getBatch(),"Honours",280,400,10,BitmapFont.HAlignment.LEFT);

        for (Medal medal:MedalManager.getHonours()) {
            medal.setPosition(300,y);
            medal.render(graphics);
            y -= 20;
        }

        mButtonGroup.render(graphics);

        // Draw way
        graphics.end();

        if (isBackJustPressed()) {
            Gdx.app.exit();
        }

        // Allow for the draw otherwise you will get flicker.
        if (!mPaused) {
            // Update the star field
            mStarField.update(delta);
            mFederation.update(delta);
            mButtonGroup.update(delta);
        }
    }

    @Override
    public void init() {
        mStarField.init();
    }

    @Override
    public boolean tap(Vector2 position) {
        return mButtonGroup.tap(position);
    }

    @Override
    public void dispose() {
    }

}
