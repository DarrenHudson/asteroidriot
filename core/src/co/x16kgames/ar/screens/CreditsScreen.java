package co.x16kgames.ar.screens;

import co.x16kgames.ar.Assets;
import co.x16kgames.common.AbstractGame;
import co.x16kgames.common.Graphics;
import co.x16kgames.common.Utils;
import co.x16kgames.common.screens.AbstractGameScreen;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;

/**
 * Credits Screen
 *
 * @author dhudson - created 23 Sep 2014
 * @since 1.1
 */
public class CreditsScreen extends AbstractGameScreen {

    private String mVersion;

    private final BitmapFont mMediumFont;
    private final BitmapFont mSmallFont;
    private final BitmapFont mLargeFont;
    private final Sprite mDazHudson;

    public CreditsScreen(AbstractGame game) {
        super(game);

        mSmallFont = Assets.getGCSmallFont();
        mMediumFont = Assets.getGCMediumFont();
        mLargeFont = Assets.getGCLargeFont();
        mDazHudson = Assets.getDazHudson();
        // 20 pixels padding from the outside edge
        mDazHudson.setX(getViewportWidth()-84);
        mDazHudson.setY(500);
    }

    @Override
    public void render(Graphics graphics,float delta) {
        Assets.getHUDFont().drawMultiLine(graphics.getBatch(),mVersion,0,getViewportHeight());

        mLargeFont.drawMultiLine(graphics.getBatch(),"Credits",getHalfViewportWidth(),
            getViewportHeight()-40,
            10,BitmapFont.HAlignment.CENTER);

        mSmallFont.drawMultiLine(graphics.getBatch(),"A 16K Games Production.\nCopyright 16K Games Ltd.",
            getHalfViewportWidth(),getViewportHeight()-140,10,BitmapFont.HAlignment.CENTER);

        mMediumFont.drawMultiLine(graphics.getBatch(),"Programming :\nSound :",10,570);
        mDazHudson.draw(graphics.getBatch());

        mMediumFont.drawMultiLine(graphics.getBatch(),"Graphics :",10,460);
        mSmallFont.drawMultiLine(graphics.getBatch(),"Dorothy Wu Ky.",60,420);

        mMediumFont.drawMultiLine(graphics.getBatch(),"Testers :",10,360);
        mSmallFont.drawMultiLine(graphics.getBatch(),
            "Holly Hudson.\nNoah Clowery.\nOscar Clowery.\nCalum Clowery.",60,320,10,BitmapFont.HAlignment.LEFT);

        mMediumFont.drawMultiLine(graphics.getBatch(),"Special thanks to :",10,200);
        mSmallFont.drawMultiLine(graphics.getBatch(),"Isaac Newton.\nJohannes Kepler.\nAngry Adam.",60,160,
            10,BitmapFont.HAlignment.LEFT);

        graphics.end();

        if (isBackJustPressed()) {
            getScreenManager().showMenuScreen();
        }
    }

    @Override
    public void dispose() {
    }

    @Override
    public boolean tap(float x,float y,int count,int button) {
        getScreenManager().showMenuScreen();
        return true;
    }

    @Override
    public void create() {
        mVersion = mGame.mRequestHandler.getVersion();
        Utils.debug("Version "+mVersion);
    }
}
