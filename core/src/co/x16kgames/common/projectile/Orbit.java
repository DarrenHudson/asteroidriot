package co.x16kgames.common.projectile;

import co.x16kgames.common.Angle;
import co.x16kgames.common.GamePreferences;
import co.x16kgames.common.GameStatus;
import co.x16kgames.common.Utils;
import co.x16kgames.common.medals.MedalManager;
import co.x16kgames.common.planets.AbstractPlanet;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

/**
 * Used to generate orbits
 *
 * @author dhudson - created 30 Sep 2014
 * @since 1.1
 */
public class Orbit {

    private static final int AWARD_MEDAL_VALUE = 500;

    /**
     * If in orbit, which body are we orbiting
     */
    private AbstractPlanet mAscendingBody;
    private final Angle mAngle;
    private final Vector2 mLastLocation;

    private float mOrbitRadius;
    private float mOrbitSpeed;

    private final Vector2 mLocation;
    private final Vector2 mVelocity;

    private float mLastDelta;

    private int mOrbitCount;

    boolean inBoost;

    private int mMoonDodging;
    private boolean hasMedal;

    public Orbit() {
        mLastLocation = new Vector2();
        mLocation = new Vector2();
        mVelocity = new Vector2();

        mAngle = new Angle(0);
    }

    public void init() {
        mAngle.setAngle(0);
        mAscendingBody = null;
        mOrbitCount = 0;
        inBoost = false;
        mMoonDodging = 0;

        hasMedal = GamePreferences.hasMedal(MedalManager.MOON_DODGER);
    }

    public void update(float delta) {
        mLastLocation.set(mLocation);
        mLastDelta = delta;
        mOrbitCount++;

        if (!hasMedal||!inBoost) {
            mMoonDodging += mAscendingBody.getNumberOfOrbitingBodies();

            if (mMoonDodging>=AWARD_MEDAL_VALUE) {
                GameStatus.awardMedal(MedalManager.MOON_DODGER);
                hasMedal = true;
            }
        }

        mAngle.increment(mOrbitSpeed);
        float radians = mAngle.toRadians();

        // Set the point to the centre of the projectile.
        mLocation.x =
            (mAscendingBody.getCentreLocation().x+mOrbitRadius*MathUtils.cos(radians))-
                Projectile.HALF_PROJECTILE_SIZE;
        mLocation.y =
            (mAscendingBody.getCentreLocation().y+mOrbitRadius*MathUtils.sin(radians))-
                Projectile.HALF_PROJECTILE_SIZE;
    }

    public int getOrbitCount() {
        return mOrbitCount;
    }

    public void setOrbitSpeed(float velocity,Vector2 projectileVelocity) {

        // Going up
        if (projectileVelocity.y>=0) {
            if (MathUtils.cosDeg(mAngle.getAngle())>0) {
                Utils.debug("Option 1");
                mOrbitSpeed = velocity;
            }
            else {
                Utils.debug("Option 2");
                mOrbitSpeed = -velocity;
            }
        }
        else {
            // Going down
            if (MathUtils.cosDeg(mAngle.getAngle())<=0) {
                Utils.debug("Option 3");
                mOrbitSpeed = -velocity;
            }
            else {
                Utils.debug("Option 4");
                mOrbitSpeed = velocity;
            }
        }
    }

    public void setOrbitRadius(float radius) {
        mOrbitRadius = radius;
    }

    /**
     * This can be positive or negative.
     *
     * @param delta
     * @since 1.1
     */
    public void addOrbitOffset(float delta) {
        mOrbitRadius += delta;
    }

    public float getRadius() {
        return mOrbitRadius;
    }

    public Vector2 getVelocity(Vector2 location) {
        mVelocity.x = MathUtils.clamp((location.x-mLastLocation.x)/mLastDelta,-100,100);
        mVelocity.y = MathUtils.clamp((location.y-mLastLocation.y)/mLastDelta,-100,100);
        return mVelocity;
    }

    public Vector2 getLocation() {
        return mLocation;
    }

    public void setAscendingBody(AbstractPlanet ascendingBody) {
        mAscendingBody = ascendingBody;
    }

    public void setAngle(Vector2 mLocation) {
        mAngle.setAngle(mAscendingBody.calculateAngle(mLocation));
    }

    /**
     * Return the ascending node name.
     *
     * @return ascending node name or null if not in orbit or landing.
     */
    public String getAscendingNodeName() {
        if (mAscendingBody!=null) {
            return mAscendingBody.getName();
        }

        return null;
    }

    /**
     * Return the ascending node.
     *
     * @return body or null.
     * @since 1.1
     */
    public AbstractPlanet getAscendingNode() {
        return mAscendingBody;
    }
}
