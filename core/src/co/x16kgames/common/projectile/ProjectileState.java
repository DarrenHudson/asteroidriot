package co.x16kgames.common.projectile;

/**
 * State of the projectile.
 * 
 * @author dhudson - created 7 Mar 2014
 * @since 1.0
 */
public enum ProjectileState {

    DOCKED,
    
    INFLIGHT,

    INORBIT,

    BOOST,

    LANDING,

    STOPPED;

    private ProjectileState() {
    }

}
