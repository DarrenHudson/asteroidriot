package co.x16kgames.ar.screens;

import co.x16kgames.common.screens.AbstractGameScreen;
import co.x16kgames.common.Galaxy;
import co.x16kgames.common.GameCamera;
import co.x16kgames.common.GamePreferences;
import co.x16kgames.common.GameStatus;
import co.x16kgames.common.GameStatus.Status;
import co.x16kgames.common.Graphics;
import co.x16kgames.common.Platform;
import co.x16kgames.common.Utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Camera;
import co.x16kgames.common.AbstractGame;

/**
 * Main game screen.
 *
 * @author dhudson - 26-Aug-2014
 * @since 1.1
 */
public class GameScreen extends AbstractGameScreen {

    public final Platform mPlatform;
    final HUDScreen mHUD;
    public GameCamera mCamera;

    public GameScreen(final AbstractGame game) {
        super(game);

        // Reset level data
        Galaxy.init();

        mPlatform = new Platform();
        // Need to set the camera for the platform
        mPlatform.setCamera(mCamera);

        mCamera.init();
        mPlatform.init();

        GameStatus.init();

        Galaxy.populateGalaxy();

        // Order important here.
        mHUD = new HUDScreen(game, this);

        // The Heads Up Display needs to share the same input
        addInputProcessor(mHUD.mDectector);
    }

    @Override
    public void render(Graphics graphics, float delta) {

        Galaxy.render(graphics);
        mPlatform.render(graphics);
        mHUD.render(graphics);
        graphics.end();

        // Sprite batches can not be nested.. Stage has its own sprite batch
        mHUD.drawStage(delta);

        // Allow for the draw otherwise you will get flicker.
        if (!mPaused) {
            Galaxy.update(delta);
            mPlatform.update(delta);
            mHUD.update(delta);

            Galaxy.interact(mPlatform.getProjectile());
            mPlatform.interact(mPlatform.getProjectile());

            // Fire on spacebar
            if (isSpaceJustPressed()) {
                // Don't allow to fire if info panel being displayed
                if (!GameStatus.isPaused()) {
                    mPlatform.fire();
                }
            }
            updateGameStatus(delta);
        }

        if (isBackJustPressed()) {
            getScreenManager().showMenuScreen();
        }

        if (Utils.isDebugging()) {
            processCheatKeys();
        }

    }

    private void updateGameStatus(float delta) {
        switch (GameStatus.getStatus()) {
            case PANNING_TO_PLANET:
                if (GameStatus.isPaused()) {
                    mHUD.displayStatusWindow();
                    GameStatus.reduceWindowTime(delta);
                } else {
                    GameStatus.setStatus(Status.WAITING_TO_LAUNCH);
                    mHUD.hideStatusWindow();
                }
                break;

            case LEVEL_COMPLETE:
                GamePreferences.incLevelsComplete();
                Galaxy.levelUp();
                Galaxy.populateGalaxy();
                
                if(mPlatform.getProjectile().getFuelRemainingAsPercent() == 100) {
                    // They have landed without using fuel!
                    GameStatus.incrementRemainingProjectiles();
                }
                
                mPlatform.init();
                mCamera.init();
                GameStatus.setStatus(Status.PANNING_TO_PLANET);

                break;

            case GAME_OVER:
                getScreenManager().showGameOverScreen();
                // Display Game over screen
                break;

            case INFO_PANEL:
                if (GameStatus.isPaused()) {
                    mHUD.displayStatusWindow();
                    GameStatus.reduceWindowTime(delta);
                } else {
                    GameStatus.resetInfoPanel();
                    mHUD.hideStatusWindow();
                }
                break;

            case IN_FLIGHT:
            case WAITING_TO_LAUNCH:
                break;

            case PROJECTILE_CRASHED:
                if (GameStatus.isPaused()) {
                    mHUD.displayStatusWindow();
                    GameStatus.reduceWindowTime(delta);
                } else {
                    mHUD.hideStatusWindow();
                    if (GameStatus.crashedIntoPlatform()) {
                        GameStatus.setGameOver();
                    } else if (GameStatus.getRemainingProjectiles() < 0) {
                        GamePreferences.incLevelsPlayed();
                        GameStatus.setGameOver();
                    } else {
                        mPlatform.getProjectile().init();
                        GameStatus.setStatus(Status.WAITING_TO_LAUNCH);
                    }
                }
                break;
        }
    }

    @Override
    public void processCheatKeys() {
        super.processCheatKeys();

        if (Gdx.input.isKeyJustPressed(Input.Keys.L)) {
            Utils.debug("Level up...");
            GameStatus.setStatus(Status.LEVEL_COMPLETE);
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.K)) {
            mPlatform.getProjectile().setDead(true);
            GameStatus.setStatus(Status.WAITING_TO_LAUNCH);
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.O)) {
            GameStatus.setGameOver();
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.C)) {
            GamePreferences.resetAll();
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.S)) {
            mPlatform.getProjectile().init();
            GameStatus.init();
            GameStatus.resetLevel();

            Galaxy.init();
            mCamera.init();
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.NUM_1)) {
            GameStatus.awardMedal(21);
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.NUM_2)) {
            GameStatus.awardMedal(31);
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.NUM_3)) {
            GameStatus.awardMedal(41);
        }
    }

    @Override
    public void init() {
        GameStatus.init();
        Galaxy.init();
        Galaxy.populateGalaxy();
        mPlatform.init();
        mCamera.init();
    }

    @Override
    public void resize(int width, int height) {
        mHUD.resize(width, height);
        mCamera.init();
        mCamera.updateViewport(width, height);
    }

    @Override
    public void dispose() {
        mHUD.dispose();
    }

    @Override
    public Camera createCamera() {
        mCamera = new GameCamera(this);
        return mCamera;
    }

    @Override
    public void create() {
    }
}
