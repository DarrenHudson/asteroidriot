package co.x16kgames.common.planets;

import co.x16kgames.ar.Assets;
import co.x16kgames.common.Galaxy;
import co.x16kgames.common.GameStatus;
import co.x16kgames.common.Utils;
import co.x16kgames.common.projectile.Projectile;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * Safe Planet
 *
 * @author dhudson - created 3 Mar 2014
 * @since 1.0
 */
public class SafePlanet extends AbstractPlanet {

    private static final long serialVersionUID = 1L;

    private static final float MAX_RADIUS = 45;
    
    /**
     * Constructor.
     *
     * @since 1.0
     */
    public SafePlanet() {
        super(Assets.getSafePlanetIcon());
    }

    /**
     * Generate the safe planet.
     *
     * @since 1.1
     */
    @Override
    public void init() {
        setBodyName();

        // This sets the size in pixels of the plant
        radius = (int)MAX_RADIUS - GameStatus.mLevel;
        configureSprite();

        // Set up the mass for gravitational effect
        setMass(radius);

        changeLocation(Galaxy.mGalaxyBounds);
    }

    /**
     * Create the shape and image from the size and location.
     *
     * @param size of safe planet
     * @param location of safe planet
     * @since 1.0
     */
    public void calculateImageShape(float size,Vector2 location) {
        radius = size;
        x = location.x;
        y = location.y;

        configureSprite();
    }

    @Override
    public void projectileHit(Projectile projectile) {
        if (!projectile.isLanding()) {
            projectile.setDead(false);
            GameStatus.setLevelComplete();
        }
    }

    @Override
    public String getType() {
        return "Safe Planet";
    }

    @Override
    public void placeInOrbit(float distance,Projectile projectile) {

        if (distance>ORBIT_DISTANCE) {
            return;
        }

        if (projectile.isInFlight()) {
            projectile.setLanding(this,distance);
        }
    }

    @Override
    public void changeLocation(Rectangle bounds) {
        // Position in the top 6th of the screen
        float section = bounds.height/6;
        mTmpRect.x = radius*2;
        mTmpRect.y = bounds.height-section;
        mTmpRect.width = bounds.width-mTmpRect.x;
        // Top quarter of the screen only
        mTmpRect.height = section-mTmpRect.x;

        setLocation(Utils.getRandomPoint(mTmpRect));
    }

    @Override
    public boolean canHaveMoons() {
        return false;
    }

}
