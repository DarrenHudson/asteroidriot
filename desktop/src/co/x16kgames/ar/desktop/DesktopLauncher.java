package co.x16kgames.ar.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import co.x16kgames.ar.AsteroidRiot;

public class DesktopLauncher {
    public static void main(String[] arg) {
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.width = 480;
        config.height = 800;
        config.title = "Asteroid Riot";
        new LwjglApplication(new AsteroidRiot(new DesktopRequestHandler()),config);
    }
}
