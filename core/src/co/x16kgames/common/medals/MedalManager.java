package co.x16kgames.common.medals;

import co.x16kgames.common.GamePreferences;

import java.io.IOException;
import java.io.Reader;
import java.util.Collection;
import java.util.HashMap;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.utils.Array;

/**
 * Medal Manager.
 * 
 * Rank medals are in the range from 1-20, the highest number being the highest rank.
 * 
 * Honours are everything above 20
 *
 * @author dhudson - created 24 Sep 2014
 * @since 1.1
 */
public class MedalManager {

    public static final int MOON_DODGER = 21;
    public static final int STAR_TRECKER = 31;
    public static final int ASTEROID_SMASHER = 41;
    public static final int PLANET_HOPPER = 51;
    public static final int SURVIVOR = 55;
    public static final int GRAVITY_MASTER = 60;
    
    private static final String MEDAL_DEFS = "medals.txt";

    private static HashMap<Integer,Medal> mMedalCache;
    static HashMap<String,Color> mColourMap;
    private static Array<Medal> mRanks;
    private static Array<Medal> mHonours;

    public static void load() throws IOException {

        mMedalCache = new HashMap<Integer,Medal>(5);
        mColourMap = new HashMap<String,Color>();
        mRanks = new Array<Medal>();
        mHonours = new Array<Medal>();

        loadColourMap();

        FileHandle file = Gdx.files.internal(MEDAL_DEFS);
        Reader reader = file.reader();

        final StringBuilder builder = new StringBuilder(20);

        int c;
        while ((c = reader.read())>=0) {
            final char ch = (char)c;
            if (ch=='\n') {
                Medal medal = new Medal(builder.toString());
                mMedalCache.put(medal.getID(),medal);
                builder.setLength(0);

                if (medal.getID()<=20) {
                    mRanks.add(medal);
                }
                else {
                    mHonours.add(medal);
                }
            }
            else {
                builder.append(ch);
            }
        }

        reader.close();
    }

    private static void loadColourMap() {
        mColourMap.put("white",Color.WHITE);
        mColourMap.put("black",Color.BLACK);
        mColourMap.put("red",Color.RED);
        mColourMap.put("green",Color.GREEN);
        mColourMap.put("blue",Color.BLUE);
        mColourMap.put("light-gray",Color.LIGHT_GRAY);
        mColourMap.put("gray",Color.GRAY);
        mColourMap.put("dark-gray",Color.DARK_GRAY);
        mColourMap.put("pink",Color.PINK);
        mColourMap.put("orange",Color.ORANGE);
        mColourMap.put("yellow",Color.YELLOW);
        mColourMap.put("magenta",Color.MAGENTA);
        mColourMap.put("cyan",Color.CYAN);
        mColourMap.put("olive",Color.OLIVE);
        mColourMap.put("purple",Color.PURPLE);
        mColourMap.put("maroon",Color.MAROON);
        mColourMap.put("teal",Color.TEAL);
        mColourMap.put("navy",Color.NAVY);
    }

    public static Color getColour(String name) {
        return mColourMap.get(name);
    }

    public static Medal getMedal(int ID) {
        return mMedalCache.get(ID);
    }

    public static Collection<Medal> getAllMedals() {
        return mMedalCache.values();
    }

    public static Array<Medal> getAllRanks() {
        return mRanks;
    }

    public static Array<Medal> getAllHonours() {
        return mHonours;
    }

    public static Medal getHighestRank() {
        Medal highestRank = getMedal(1);

        for (Medal medal:GamePreferences.getMedals()) {
            if (medal.isRank()) {
                if (medal.getID()>highestRank.getID()) {
                    highestRank = medal;
                }
            }
        }

        return highestRank;
    }

    public static Array<Medal> getHonours() {
        Array<Medal> honours = new Array<Medal>();

        for (Medal medal:GamePreferences.getMedals()) {
            if (medal.isHonour()) {
                honours.add(medal);
            }
        }

        return honours;
    }
}
