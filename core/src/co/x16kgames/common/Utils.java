package co.x16kgames.common;

import co.x16kgames.ar.AsteroidRiot;
import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Peripheral;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * Collection of static methods.
 *
 * @author dhudson - 28-Aug-2014
 * @since 1.1
 */
public class Utils {

    public static final Rectangle EMPTY_RECT = new Rectangle(0,0,0,0);

    /**
     * Private Constructor.
     */
    private Utils() {
    }

    /**
     * Log a debug message.
     *
     * @param message to display
     * @since 1.1
     */
    public static void debug(String message) {
        if(isDebugging()) {
            Gdx.app.debug("16K:Asteroid Riot",message);
        }
    }

    /**
     * Check to see if the log level is debugging.
     *
     * @return true if the log level is debugging.
     * @since 1.1
     */
    public static boolean isDebugging() {
        return AsteroidRiot.isDebugging;
    }

    /**
     * Return the application type.
     *
     * @return application type.
     * @since 1.1
     */
    public static ApplicationType getPlatformType() {
        return Gdx.app.getType();
    }

    /**
     * Set a random point in Rect using bounds.
     *
     * @param bounds
     * @param rect
     * @since 1.1
     */
    public static void setRandomPoint(Rectangle bounds,Rectangle rect) {
        rect.x = MathUtils.random(bounds.x,bounds.width);
        rect.y = MathUtils.random(bounds.y,bounds.height);
    }

    /**
     * Return a random point within the given dimension.
     *
     * @param rect of which the point must be contained
     * @return a random point within the dimension
     * @since 1.0
     */
    public static Vector2 getRandomPoint(Rectangle rect) {
        return new Vector2(MathUtils.random(rect.x,rect.width),
            MathUtils.random(rect.y,rect.y+rect.height));
    }

    /**
     * Generate a random colour
     *
     * @return a new colour not guaranteed to be unique
     * @since 0.1
     */
    public static Color getRandomColor() {
        return new Color(MathUtils.random(),MathUtils.random(),MathUtils.random(),1);
    }

    /**
     * Returns true if two rectangles intersect.
     *
     * @param a the first rectangle.
     * @param b the second rectangle.
     * @return true if the rectangles intersect, otherwise false.
     */
    public static boolean intersects(Rectangle a,Rectangle b) {
        return (a.x+a.width>b.x)&&(a.x<b.x+b.width)&&(a.y+a.height>b.y)&&(a.y<b.y+b.height);
    }

    /**
     * Check to see if the accelerometer is available.
     * 
     * @return true if the accelerometer is available
     * @since 1.1
     */
    public static boolean isAcceleromterAvailable() {
        return Gdx.input.isPeripheralAvailable(Peripheral.Accelerometer);
    }

    /**
     * Returns the absolute value of a {@code float} value. If the argument is not negative, the argument is returned.
     * If the argument is negative, the negation of the argument is returned. Special cases:
     * <ul>
     * <li>If the argument is positive zero or negative zero, the result is positive zero.
     * <li>If the argument is infinite, the result is positive infinity.
     * <li>If the argument is NaN, the result is NaN.
     * </ul>
     * In other words, the result is the same as the value of the expression:
     * <p>
     * {@code Float.intBitsToFloat(0x7fffffff & Float.floatToIntBits(a))}
     *
     * @param a the argument whose absolute value is to be determined
     * @return the absolute value of the argument.
     * @since 1.1
     */
    public static float abs(float a) {
        return (a<=0.0F) ? 0.0F-a : a;
    }

    // Used with formatFloat
    private static final StringBuilder mBuilder = new StringBuilder();
    private static int mIndex = 0;

    /**
     * Return a String representation of a float.
     *
     * GWT doesn't support String.format or Decimal format
     *
     * @param number to format
     * @return formatted number
     * @since 1.1
     */
    public static String formatFloat(float number) {
        mBuilder.setLength(0);
        mBuilder.append(number);
        mIndex = mBuilder.indexOf(".");
        if (mIndex>0) {
            if (mIndex+3<=mBuilder.length()) {
                mBuilder.setLength(mIndex+3);
            }
        }

        return mBuilder.toString();
    }

    /**
     * Linearly interpolates between fromValue to toValue on progress position.
     *
     * @param fromValue
     * @param toValue
     * @param progress value from 0-1 as a percentage
     * @return the delta and not the position
     * @since 1.1
     */
    public static float deltaLerp(float fromValue,float toValue,float progress) {
        return (toValue-fromValue)*progress;
    }

}
