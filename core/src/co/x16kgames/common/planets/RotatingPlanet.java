package co.x16kgames.common.planets;

import co.x16kgames.common.Angle;
import co.x16kgames.common.Graphics;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;

/**
 * Rotating Planet
 *
 * @author dhudson - created 10 Oct 2014
 * @since 1.1
 */
public abstract class RotatingPlanet extends AbstractPlanet {

    private static final long serialVersionUID = 1L;

    private final Angle mAngle;
    // The rotate step
    private float mSpeed;

    public RotatingPlanet(Sprite sprite) {
        super(sprite);
        mAngle = new Angle(0);
    }

    @Override
    public void render(Graphics graphics) {
        getSprite().setRotation(mAngle.getAngle());
        super.render(graphics);
    }

    /**
     * Sets name, random speed and angle.
     * 
     * @see gravitysucks.planets.Body#init()
     */
    public void init() {
        super.init();
        mSpeed = MathUtils.random(-100f,100f);
        mAngle.setAngle(MathUtils.random(360f));
    }

    @Override
    public void update(float delta) {
        super.update(delta);
        mAngle.increment(mSpeed*delta);
    }

}
