package co.x16kgames.common.lw;

import co.x16kgames.ar.Assets;
import co.x16kgames.common.Graphics;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFont.TextBounds;

/**
 * Button with Text.
 *
 * @author dhudson - created 22 Sep 2014
 * @since 1.0
 */
public class LWTextButton extends LWButton {

    private final CharSequence mText;
    private boolean isDirty;

    private float mXOffset = 0;
    private float mYOffset = 0;

    private static final BitmapFont mFont = Assets.getHUDFont();

    public LWTextButton(CharSequence text) {
        mText = text;
        isDirty = true;
    }

    @Override
    public void render(Graphics graphics) {
        // Draw the background
        super.render(graphics);

        if (isDirty) {
            calculateOffsets();
        }

        // Could make faster by only calculating this once.
        mFont.drawMultiLine(graphics.getBatch(),mText,mX+mXOffset,mY+(mHeight-mYOffset));
    }

    private void calculateOffsets() {
        TextBounds bounds = mFont.getBounds(mText);

        // 4 seems to be the padding for this font.
        mXOffset = (mWidth-bounds.width)/2-4;
        mYOffset = ((mHeight-bounds.height)/2)-4;

        isDirty = false;
    }
}

