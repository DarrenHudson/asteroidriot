package co.x16kgames.common;

import co.x16kgames.ar.Assets;

/**
 * Handles the playing of sounds
 *
 * @author dhudson - created 3 Oct 2014
 * @since 1.1
 */
public class SoundManager {

    public static void playExplosion() {
        if (GamePreferences.isSoundRequired) {
            Assets.getExplosionSound().play(GamePreferences.mSoundVolume);
        }
    }

    public static void playBounce() {
        if (GamePreferences.isSoundRequired) {
            Assets.getBounceSound().play(GamePreferences.mSoundVolume);
        }
    }

    public static void playLaunch() {
        if (GamePreferences.isSoundRequired) {
            Assets.getLaunchSound().play(GamePreferences.mSoundVolume);
        }
    }
}
