package co.x16kgames.common;

import co.x16kgames.ar.Assets;
import co.x16kgames.common.projectile.Projectile;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

/**
 * Shooting platform.
 *
 * For now this is at the bottom of the screen, but could be placed on a planet.
 *
 * @author dhudson - created 23 Feb 2014
 * @since 1.0
 */
public final class Platform extends MovingGameObject {

    private final Vector3 touchPos = new Vector3();

    // Aspect Ratio of 1.32
    public static final int PLATFORM_HEIGHT = 48;
    public static final int PLATFORM_WIDTH = 64;

    // Can only have one
    private final Projectile mProjectile;
    private Sprite mSprite;
    private final Sprite mLighteningSprite1;
    private final Sprite mLighteningSprite2;
    private final Sprite mNormalSprite;

    private GameCamera mCamera;

    private final boolean mAccelerometerAvailable;
    private float mElapsedTime;
    private float mFrameDuration = 1f;

    /**
     * Constructor.
     *
     * @since 1.0
     */
    public Platform() {

        mAccelerometerAvailable = Utils.isAcceleromterAvailable()&&GamePreferences.isAccelerometerRequired;

        // One projectile
        mProjectile = new Projectile(this);

        mLighteningSprite1 = Assets.getShipLightening1();
        configureSprite(mLighteningSprite1,PLATFORM_WIDTH,PLATFORM_HEIGHT);

        mLighteningSprite2 = Assets.getShipLightening2();
        configureSprite(mLighteningSprite2,PLATFORM_WIDTH,PLATFORM_HEIGHT);

        mNormalSprite = Assets.getShipNormal();
        configureSprite(mNormalSprite,PLATFORM_WIDTH,PLATFORM_HEIGHT);

        mSprite = mLighteningSprite1;
    }

    public void setCamera(GameCamera gameCamera) {
        mCamera = gameCamera;
    }

    @Override
    public void render(Graphics graphics) {
        if (!isDead) {
            graphics.drawSprite(mSprite,mLocation);
        }

        mProjectile.render(graphics);
    }

    @Override
    public void update(float delta) {

        setVelocity(0,0);

        if (Gdx.input.isTouched()) {
            touchPos.set(Gdx.input.getX(),Gdx.input.getY(),0);
            mCamera.unproject(touchPos);

            // If the touch is at the bottom part of the screen
            if (touchPos.y<=PLATFORM_HEIGHT) {
                float spriteMidPoint = mLocation.x+(PLATFORM_WIDTH/2);
                if (touchPos.x>spriteMidPoint) {
                    setXVelocity(200);
                }

                if (touchPos.x<spriteMidPoint) {
                    setXVelocity(-200);
                }
            }
        }

        // See if they are tipping the screen
        if (mAccelerometerAvailable) {
            // Lets have some tolerance ..
            float accel = Gdx.input.getAccelerometerX();

            if (Utils.abs(accel)>GamePreferences.mAccelTollerance) {
                mLocation.x -= accel;
            }
        }

        if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
            setXVelocity(-200);
        }
        else if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
            setXVelocity(200);
        }

        // Act upon the x velocity.
        super.update(delta);

        // make sure the platform stays within the screen bounds
        if (mLocation.x<0) {
            mLocation.x = 0;
        }

        if (mLocation.x>Galaxy.mWidth-PLATFORM_WIDTH) {
            mLocation.x = Galaxy.mWidth-PLATFORM_WIDTH;
        }

        updateSprite(delta);

        // Pan if required
        mCamera.checkPlatform();

        mProjectile.update(delta);
    }

    public void updateSprite(float delta) {
        if (mProjectile.isDocked()) {

            mElapsedTime += delta;
            if (mElapsedTime>mFrameDuration) {
                mElapsedTime = 0;
                mFrameDuration = getFrameDuration();
                if (mSprite==mLighteningSprite2) {
                    mSprite = mLighteningSprite1;
                }
                else {
                    mSprite = mLighteningSprite2;
                }
            }
        }
        else {
            mSprite = mNormalSprite;
        }
    }

    private float getFrameDuration() {
        return MathUtils.random(0.3f,1.5f);
    }

    public Vector2 getCenter(Vector2 centreLocation) {
        centreLocation.y = mLocation.y+(PLATFORM_HEIGHT/2);
        centreLocation.x = mLocation.x+(PLATFORM_WIDTH/2);
        return centreLocation;
    }

    @Override
    public boolean intersects(Rectangle rect) {
        return mSprite.getBoundingRectangle().overlaps(rect);
    }

    @Override
    public void interact(Projectile projectile) {
        if (projectile.isDead()) {
            return;
        }

        if (!projectile.isDocked()) {
            if (intersects(projectile.getBounds())) {
                mProjectile.setDead(true);
                GameStatus.setCrashedIntoPlatform();
                isDead = true;
            }
        }
    }

    @Override
    public boolean isDead() {
        return false;
    }

    @Override
    public void init() {
        isDead = false;
        mProjectile.init();
        mLocation.x = Galaxy.mWidth/2;
        mElapsedTime = 0;
        mFrameDuration = getFrameDuration();
    }

    /**
     * Check to see if its possible to launch a projectile.
     *
     * @return true if projectile launched.
     */
    public boolean fire() {

        // Only one at a time..
        if (mProjectile.isDocked()) {

            // mProjectile.init();
            mProjectile.setInFlight();
            SoundManager.playLaunch();
            return true;
        }

        if (mProjectile.isInOrbit()) {
            mProjectile.boost();
            SoundManager.playLaunch();
            return true;
        }

        return false;
    }

    /**
     * Return the projectile.
     *
     * @return projectile, which may be dead
     * @since 1.1
     */
    public Projectile getProjectile() {
        return mProjectile;
    }

    @Override
    public Rectangle getBounds() {
        return mSprite.getBoundingRectangle();
    }
}
