package co.x16kgames.common.explosion;

import co.x16kgames.common.Graphics;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;

/**
 * Firework details.
 *
 * @author dhudson - created 9 Oct 2014
 * @since 1.1
 */
public class Firework {

    private final Explosion mExplosion;
    private float mDelay;
    private int mCount;
    private final Vector2 mLocation;

    public Firework(float x,float y,Color colour,float delay) {
        mLocation = new Vector2(x,y);
        mExplosion = new Explosion(300,colour);
        mDelay = delay;
    }

    public void init() {
        mExplosion.setDead();
        mCount = 0;
    }

    public void update(float delta) {
        mCount++;
        if (mCount==mDelay) {
            mExplosion.init(mLocation);
        }

        mExplosion.update(delta);
    }

    public void render(Graphics graphics) {
        mExplosion.render(graphics);
    }

    public void setDead() {
        mExplosion.setDead();
    }
}
