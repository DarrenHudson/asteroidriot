package co.x16kgames.common;

import co.x16kgames.common.projectile.Projectile;

import com.badlogic.gdx.math.Rectangle;

/**
 * Interface of items on the Canvas / Universe.
 *
 * @author dhudson - created 1 Mar 2014
 * @since 1.0
 */
public interface Drawable {

    /**
     * When this is called, the implementing method needs to render itself and any children.
     *
     * @param graphics to use
     * @since 1.0
     */
    public void render(Graphics graphics);

    /**
     * Check to see if the rect intersets drawn image.
     *
     * @param rect to check
     * @return true if the given rect intersets what is to be drawn.
     * @since 1.0
     */
    public boolean intersects(Rectangle rect);

    /**
     * Called after render.
     * 
     * @param delta time since last called
     *
     * @since 1.0
     */
    public void update(float delta);

    /**
     * Interact with the projectile.
     *
     * @param projectile current projectile
     * @since 1.0
     */
    public void interact(Projectile projectile);

    /**
     * Check to see if the drawable needs to be removed.
     *
     * @return true if to be moved from the Universe
     * @since 1.0
     */
    public boolean isDead();

    /**
     * Init the drawable so that it can be reused.
     * 
     * Init should be called from the constructor.
     * 
     * @since 1.1
     */
    public void init();

    /**
     * Set the location of the drawable
     * 
     * @param x
     * @param y
     */
    public void setLocation(float x,float y);

    /**
     * Return the bounds of the drawable.
     * 
     * @return bounds
     */
    public Rectangle getBounds();

}
