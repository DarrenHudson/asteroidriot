package co.x16kgames.common;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;

/**
 * Class to wrap graphic aspects of the game.
 *
 * @author dhudson - 28-Aug-2014
 * @since 1.0
 */
public class Graphics {

    private final SpriteBatch mBatch;
    
    public Graphics() {
        mBatch = new SpriteBatch();
    }

    /**
     * Camera update needs to be done before this.
     *
     * @param cameraMatrix
     */
    public void begin(Matrix4 cameraMatrix) {
        // Clear screen to black
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        mBatch.setProjectionMatrix(cameraMatrix);
        mBatch.begin();
    }

    public void drawSprite(Sprite sprite, float x, float y) {
        sprite.setX(x);
        sprite.setY(y);
        sprite.draw(mBatch);
    }

    public void drawSprite(Sprite sprite, Vector2 location) {
        sprite.setX(location.x);
        sprite.setY(location.y);
        sprite.draw(mBatch);
    }

    public void drawSprite(Sprite sprite, Vector2 location, Color colour) {
        mBatch.setColor(colour);
        drawSprite(sprite, location);
        resetColor();
    }

    public void draw(TextureRegion region, float x, float y) {
        mBatch.draw(region, x, y);
    }

    public void draw(Texture texture, float x, float y) {
        mBatch.draw(texture, x, y);
    }

    public void drawString(BitmapFont font, String string, int x, int y) {
        font.draw(mBatch, string, x, y);
    }

    public void end() {
        mBatch.end();
    }

    public SpriteBatch getBatch() {
        return mBatch;
    }

    public void setColor(Color color) {
        mBatch.setColor(color);
    }

    public void resetColor() {
        mBatch.setColor(Color.WHITE);
    }

    public void dispose() {
        mBatch.dispose();
    }

}
