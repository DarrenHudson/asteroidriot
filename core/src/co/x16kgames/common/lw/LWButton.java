package co.x16kgames.common.lw;

import co.x16kgames.ar.Assets;
import co.x16kgames.common.Graphics;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

/**
 * This class represents a light weight button.
 * 
 * This does not need a stage and a faster implementation of the Button class.
 *
 *
 * @author dhudson - created 22 Sep 2014
 * @since 1.1
 */
public class LWButton {

    private final com.badlogic.gdx.scenes.scene2d.utils.Drawable mBackground;
    float mX;
    float mY;
    float mWidth;
    float mHeight;

    private LWButtonListener mListener;

    boolean isVisible;

    public LWButton() {
        mBackground = Assets.getDefaultSkin().getDrawable("default-pane");
        mX = 100;
        mY = 400;
        mWidth = 100;
        mHeight = 100;
        isVisible = true;
    }

    public LWButton setLocation(float x,float y) {
        mX = x;
        mY = y;
        return this;
    }

    public LWButton setWidth(float width) {
        mWidth = width;
        return this;
    }

    public LWButton setHeight(float height) {
        mHeight = height;
        return this;
    }

    public void render(Graphics graphics) {
        if (isVisible) {
            SpriteBatch batch = graphics.getBatch();

            batch.setColor(1,1,1,0.5f);
            mBackground.draw(batch,mX,mY,mWidth,mHeight);
            batch.setColor(1,1,1,1);
        }
    }

    public LWButton setListener(LWButtonListener listener) {
        mListener = listener;
        return this;
    }

    public LWButtonListener getListener() {
        return mListener;
    }

    public void notifyListener(Vector2 location) {
        if (mListener!=null) {
            mListener.touched(location);
        }
    }

    public void update(float delta) {
    }

    /**
     * Check to see if the point is contained in this button.
     * 
     * @param x point x coordinate
     * @param y point y coordinate
     * @return whether the point is contained in the rectangle
     */
    public boolean contains(float x,float y) {
        return mX<=x&&mX+mWidth>=x&&mY<=y&&mY+mHeight>=y&&isVisible;
    }

    public boolean contains(Vector2 position) {
        return contains(position.x,position.y);
    }
    
    public void setVisible(boolean visible) {
        isVisible = visible;
    }
}
