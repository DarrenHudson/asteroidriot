package co.x16kgames.common.panels;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.scenes.scene2d.utils.Align;

/**
 * Crashed Panel.
 *
 * @author dhudson - 16-Sep-2014
 * @since 1.1
 */
public class CrashedPanel extends AbstractPanel {

    public CrashedPanel(Camera camera) {
        super(camera);
        mRoot.add(createLabel("Infinity")).colspan(3).pad(10).align(Align.center).row();
    }

    @Override
    public void populate() {
        packAndCentre();
    }

}
