package co.x16kgames.common.planets;

import co.x16kgames.common.Angle;
import co.x16kgames.common.Graphics;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;

/**
 * The body rotates.
 *
 * @author dhudson - 18-Sep-2014
 * @since
 */
public abstract class RotatingBody extends Body {

    private final Angle mAngle;

    // The rotate step
    float mSpeed;

    public RotatingBody(Sprite sprite) {
        super(sprite);
        mAngle = new Angle();
    }

    @Override
    public void render(Graphics graphics) {
        getSprite().setRotation(mAngle.getAngle());
        drawSprite(graphics);
    }

    /**
     * Sets name, random speed and angle.
     * 
     * @see gravitysucks.planets.Body#init()
     */
    public void init() {
        setBodyName();
        mSpeed = MathUtils.random(-100f,100f);
        mAngle.setAngle(MathUtils.random(360f));
    }

    @Override
    public void update(float delta) {
        mAngle.increment(mSpeed*delta);
    }

}
