package co.x16kgames.ar.screens;

import co.x16kgames.ar.Assets;
import co.x16kgames.common.AbstractGame;
import co.x16kgames.common.Galaxy;
import co.x16kgames.common.Graphics;
import co.x16kgames.common.Platform;
import co.x16kgames.common.StarField;
import co.x16kgames.common.planets.Asteroid;
import co.x16kgames.common.planets.SafePlanet;
import co.x16kgames.common.projectile.Projectile;
import co.x16kgames.common.screens.AbstractGameScreen;
import co.x16kgames.common.screens.ScreenshotFactory;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;

/**
 * Display a Splash Screen allowing for the adverts to load.
 * 
 * This is only used for Android.
 *
 * @author dhudson - created 20 Oct 2014
 * @since 1.0
 */
public class SplashScreen extends AbstractGameScreen {

    private final float PAUSE_TIME = 5;
    private float mWindowDelay;
    private Sprite mPod;
    private Sprite mPlatform;
    private SafePlanet mSafePlanet;
    private Asteroid mAsteroid;
    private Sprite mFlame;
    private Sprite mLogoSprite;
    private Sprite mDockedSprite;

    private final StarField mStarField;

    private boolean mTakeScreenShot = false;

    public SplashScreen(AbstractGame game) {
        super(game);
        mStarField = Galaxy.mStarField;
    }

    @Override
    public void render(Graphics graphics,float delta) {
        mWindowDelay -= delta;
        if (mWindowDelay<=0) {
            getScreenManager().showMenuScreen();
        }

        mStarField.render(graphics);
        mSafePlanet.render(graphics);
        mPod.draw(graphics.getBatch());
        mFlame.draw(graphics.getBatch());
        mAsteroid.render(graphics);
        mPlatform.draw(graphics.getBatch());
        mDockedSprite.draw(graphics.getBatch());
        mLogoSprite.draw(graphics.getBatch());

        Assets.getLargeFont().drawMultiLine(graphics.getBatch(),"Asteroid\nRiot!",
            getHalfViewportWidth(),
            getHalfViewportHeight()+200,
            10,BitmapFont.HAlignment.CENTER);

        graphics.end();

        // Update bit.
        mStarField.update(delta);

        if (mTakeScreenShot) {
            ScreenshotFactory.saveScreenshot();
            mTakeScreenShot = false;
        }
    }

    @Override
    public void dispose() {
    }

    @Override
    public void init() {
        mWindowDelay = PAUSE_TIME;
    }

    @Override
    public void create() {
        mPod = Assets.getProjectileRedSprite();
        mPod.setSize(32,32);
        mPod.setOriginCenter();
        mPod.setRotation(25);

        mFlame = Assets.getFlameDown();
        mFlame.setSize(16,16);
        mFlame.rotate(25);

        mAsteroid = new Asteroid();
        mAsteroid.init();

        mPlatform = Assets.getShipLightening2();
        mPlatform.setSize(Platform.PLATFORM_WIDTH*2,Platform.PLATFORM_HEIGHT*2);
        mPlatform.setOriginCenter();

        mSafePlanet = new SafePlanet();
        mSafePlanet.init();

        mLogoSprite = Assets.getLogoSprite();
        mLogoSprite.setSize(200,97.5f);
        mLogoSprite.setOriginCenter();

        mDockedSprite = Assets.getProjectileGreenSprite();
        mDockedSprite.setSize(Projectile.PROJECTILE_SIZE*2,Projectile.PROJECTILE_SIZE*2);
    }

    @Override
    public void resize(int width,int height) {
        super.resize(width,height);
        mStarField.init();

        mPod.setX(getHalfViewportWidth()-16);
        mPod.setY(getViewportHeight()-130);

        mFlame.setX(mPod.getX()+8);
        mFlame.setY(mPod.getY()-38);

        mAsteroid.setLocation(getHalfViewportWidth()-70,getViewportHeight()-160);

        mPlatform.setX(getHalfViewportWidth()-64);
        mPlatform.setY(300);

        Vector2 location = new Vector2(getHalfViewportWidth()-70,getViewportHeight()-160);
        mSafePlanet.calculateImageShape(60,location);

        mLogoSprite.setX(getHalfViewportWidth()-(mLogoSprite.getWidth()/2));
        mLogoSprite.setY(100);

        mDockedSprite.setX(mPlatform.getX()+48);
        mDockedSprite.setY(mPlatform.getY()+48);

    }

}
