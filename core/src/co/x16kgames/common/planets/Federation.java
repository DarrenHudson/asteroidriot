package co.x16kgames.common.planets;

import java.util.Iterator;

import co.x16kgames.common.Graphics;
import co.x16kgames.common.projectile.Projectile;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Array;

/**
 * A Federation is a collections of Bodies.
 *
 * @author dhudson - created 31 Oct 2014
 * @since 4
 */
public class Federation {

    private final Rectangle mBounds;

    private final Array<Body> mBodies;
    private int mOverlapCount = 0;

    public Federation() {
        mBodies = new Array<Body>(5);
        mBounds = new Rectangle();
    }

    public void setBounds(Rectangle rect) {
        mBounds.set(rect);
    }

    public void setBounds(float x,float y,float height,float width) {
        mBounds.x = x;
        mBounds.y = y;
        mBounds.height = height;
        mBounds.width = width;
    }

    public Rectangle getBounds() {
        return mBounds;
    }

    public void render(Graphics graphics) {
        for (Body body:mBodies) {
            body.render(graphics);
        }
    }

    public void update(float delta) {
        Iterator<Body> iter = mBodies.iterator();
        while (iter.hasNext()) {
            Body body = iter.next();
            body.update(delta);
            if (body.isDead()) {
                iter.remove();
            }
        }
    }

    public void renderBounds(ShapeRenderer renderer) {
        Rectangle bounds;
        for (Body body:mBodies) {
            bounds = body.getBounds();
            renderer.rect(bounds.x,bounds.y,bounds.width,bounds.height);
        }
    }

    public void clear() {
        mBodies.clear();
    }

    public Body locateBodyAt(float x,float y) {
        for (Body body:mBodies) {
            if (body.getBounds().contains(x,y)) {
                return body;
            }
        }

        return null;
    }

    public void interact(Projectile projectile) {
        for (Body body:mBodies) {
            body.interact(projectile);
        }
    }

    public void checkAndAddBody(Body body) {
        boolean overlap = false;

        for (Body existingBody:mBodies) {
            if (existingBody.getBounds().overlaps(body.getBounds())) {
                // The bodies overlap
                overlap = true;
                break;
            }
        }

        if (overlap) {
            mOverlapCount++;
            if (mOverlapCount>50) {
                // Give up
                return;
            }

            body.changeLocation(mBounds);
            checkAndAddBody(body);
        }
        else {
            mOverlapCount = 0;
            mBodies.add(body);
        }
    }
}
