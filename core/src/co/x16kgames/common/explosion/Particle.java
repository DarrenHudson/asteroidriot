package co.x16kgames.common.explosion;

import co.x16kgames.ar.Assets;
import co.x16kgames.common.Graphics;
import co.x16kgames.common.MovingGameObject;
import co.x16kgames.common.projectile.Projectile;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * Particle, is part of the explosion.
 *
 * @author dhudson - 20-Sep-2014
 * @since 1.1
 */
public class Particle extends MovingGameObject {

    private static final int MAX_SPEED = 15;

    private boolean isDead;
    // private float mRedShade;
    private float mAlpha;
    private int mLifeSpan;
    private float mFadeRate;
    private final Color mColour;

    public Particle(Color colour) {
        mColour = colour;
    }

    public void init(Vector2 position,int maxLifeSpan) {
        isDead = false;
        setLocation(position);
        setVelocity(MathUtils.random(-MAX_SPEED,MAX_SPEED),MathUtils.random(-MAX_SPEED,MAX_SPEED));
        // mRedShade = MathUtils.random(0.5f,1f);
        mLifeSpan = MathUtils.random(maxLifeSpan-50,maxLifeSpan);
        mAlpha = 1;
        mFadeRate = 1/maxLifeSpan;
    }

    @Override
    public void render(Graphics graphics) {
        if (!isDead) {
            graphics.getBatch().setColor(mColour.r,mColour.g,mColour.b,mAlpha);
            graphics.getBatch().draw(Assets.getBlockTextureRegion(),mLocation.x,mLocation.y);
        }
    }

    @Override
    public void update(float delta) {
        if (!isDead) {
            if (--mLifeSpan<0) {
                isDead = true;
            }
            else {
                super.update(delta);
                mAlpha -= mFadeRate;
            }
        }
    }

    @Override
    public void interact(Projectile projectile) {
    }

    @Override
    public Rectangle getBounds() {
        // Not used
        return null;
    }

    @Override
    public void init() {
        // Not used
    }
}
