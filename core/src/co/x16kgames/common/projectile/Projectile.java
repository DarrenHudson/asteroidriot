package co.x16kgames.common.projectile;

import co.x16kgames.ar.Assets;
import co.x16kgames.common.Galaxy;
import co.x16kgames.common.GamePreferences;
import co.x16kgames.common.GameStatus;
import co.x16kgames.common.Graphics;
import co.x16kgames.common.MovingGameObject;
import co.x16kgames.common.Platform;
import co.x16kgames.common.SoundManager;
import co.x16kgames.common.Utils;
import co.x16kgames.common.explosion.Collision;
import co.x16kgames.common.explosion.Explosion;
import co.x16kgames.common.medals.MedalManager;
import co.x16kgames.common.planets.AbstractPlanet;
import co.x16kgames.common.planets.Body;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * Class to represent a projectile.
 *
 * @author dhudson - created 20 Feb 2014
 * @since 1.0
 */
public final class Projectile extends MovingGameObject {

    private static final int AWARD_MEDAL = 5001;

    private float mOldXOffset;

    private ProjectileState mState;
    private final Trajectory mTrajectory;

    private Sprite mSprite;
    private final Sprite mBlackSprite;
    private final Sprite mRedSprite;
    private final Sprite mDamagedSprite;
    private final Sprite mDockedSprite;

    private final Sprite mThrustLeft;
    private final Sprite mThrustRight;
    private final Sprite mThrustDown;
    private boolean mThrustingLeft;
    private boolean mThrustingRight;
    private boolean mThrustingDown;

    private float mElapsedTime;
    private final float FRAME_DURATION = 1f;

    public static final int PROJECTILE_SIZE = 16;
    public static final int HALF_PROJECTILE_SIZE = PROJECTILE_SIZE/2;

    private final Vector2 mCentrePoint;

    private float mDamage;

    private final Explosion mExplosion;

    private final Orbit mOrbit;

    private int mBoostCount;
    private int mOrbitsThisFlight;

    private float mFuelUsed;
    private float mTotalFuel;

    private final Platform mPlatform;

    private final Rectangle mBounds;

    /**
     * Constructor.
     *
     * @param platform
     * @since 1.0
     */
    public Projectile(Platform platform) {
        mPlatform = platform;
        mTrajectory = new Trajectory(this);
        mOrbit = new Orbit();
        mExplosion = new Collision();

        mDockedSprite = Assets.getProjectileGreenSprite();
        configureSprite(mDockedSprite,PROJECTILE_SIZE);

        mBlackSprite = Assets.getProjectileBlackSprite();
        configureSprite(mBlackSprite,PROJECTILE_SIZE);

        mRedSprite = Assets.getProjectileRedSprite();
        configureSprite(mRedSprite,PROJECTILE_SIZE);

        mDamagedSprite = Assets.getProjectileDamagedSprite();
        configureSprite(mDamagedSprite,PROJECTILE_SIZE);

        mThrustLeft = Assets.getFlameLeft();
        configureSprite(mThrustLeft,HALF_PROJECTILE_SIZE);

        mThrustRight = Assets.getFlameRight();
        configureSprite(mThrustRight,HALF_PROJECTILE_SIZE);

        mThrustDown = Assets.getFlameDown();
        configureSprite(mThrustDown,HALF_PROJECTILE_SIZE);

        mSprite = mBlackSprite;
        mCentrePoint = new Vector2();

        mBounds = new Rectangle(0,0,PROJECTILE_SIZE,PROJECTILE_SIZE);
    }

    @Override
    public void init() {
        setVelocity(0,100);
        mBoostCount = 0;
        mTrajectory.init();
        mOrbit.init();
        mOldXOffset = 0;
        mDamage = 0;
        mExplosion.setDead();
        mOrbitsThisFlight = 0;
        mElapsedTime = 0;
        mFuelUsed = 0;
        mTotalFuel = 200-(GameStatus.mLevel*10);
        setDocked();
    }

    @Override
    public void render(Graphics graphics) {

        if (mState!=ProjectileState.STOPPED) {
            // Draw these first
            mTrajectory.render(graphics);
            graphics.drawSprite(mSprite,mLocation);

            if (mThrustingRight) {
                graphics.drawSprite(mThrustLeft,mLocation.x-HALF_PROJECTILE_SIZE,mLocation.y+(HALF_PROJECTILE_SIZE/2));
            }

            if (mThrustingLeft) {
                graphics.drawSprite(mThrustRight,mLocation.x+PROJECTILE_SIZE,mLocation.y+(HALF_PROJECTILE_SIZE/2));
            }

            if (mThrustingDown) {
                graphics.drawSprite(mThrustDown,mLocation.x+(HALF_PROJECTILE_SIZE/2),mLocation.y-HALF_PROJECTILE_SIZE);
            }
        }

        mExplosion.render(graphics);
    }

    /**
     * Return the distance travelled by the projectile.
     *
     * @return the distance travelled
     * @since 1.1
     */
    public float getDistance() {
        return mTrajectory.getDistance();
    }

    /**
     * Update the position and velocity of the projectile.
     *
     * This depends on gravitational forces from planets or moons
     *
     * @param delta
     * @since 1.0
     */
    @Override
    public void update(float delta) {

        calculateThrust(delta);

        switch (mState) {

            case INFLIGHT:
                super.update(delta);
                checkInUniverse();
                break;

            case BOOST:
                if (--mBoostCount==0) {
                    setVelocity(mOrbit.getVelocity(mLocation));
                    mState = ProjectileState.INFLIGHT;
                }
                else {
                    mOrbit.update(delta);
                    mOrbit.addOrbitOffset(0.2f);
                    setLocation(mOrbit.getLocation());
                }
                break;

            case INORBIT:
                mOrbit.update(delta);
                setLocation(mOrbit.getLocation());
                break;

            case LANDING:
                mOrbit.update(delta);

                if (mOrbit.getRadius()<1) {

                    if (mDamage>98) {
                        GameStatus.awardMedal(MedalManager.SURVIVOR);
                    }

                    // Landed with no fuel..
                    if (mFuelUsed==0) {
                        if (GameStatus.increaseGravityMaster()) {
                            GameStatus.awardMedal(MedalManager.GRAVITY_MASTER);
                        }
                    }

                    mState = ProjectileState.STOPPED;
                    mOrbit.getAscendingNode().projectileHit(this);
                }
                else {
                    mOrbit.addOrbitOffset(-1);
                    setLocation(mOrbit.getLocation());
                }

                break;

            case DOCKED:
                mLocation.x = mPlatform.mLocation.x+24;
                break;

            case STOPPED:
                mExplosion.update(delta);
                break;
        }

        updateSprite(delta);
        mTrajectory.update(delta);
    }

    private void calculateThrust(float delta) {
        mThrustingLeft = mThrustingRight = false;
        // Can only apply thrust in flight
        if (isInFlight()) {

            if (mTrajectory.getTrajectoryCount()>40) {
                mThrustingDown = false;
            }

            if (mFuelUsed<mTotalFuel) {
                // Thrusting left
                if (Gdx.input.isKeyPressed(Input.Keys.Z)) {
                    addXVelocity(-5);
                    mFuelUsed += 5;
                    mThrustingLeft = true;
                }

                // Thrusting right
                if (Gdx.input.isKeyPressed(Input.Keys.X)) {
                    addXVelocity(5);
                    mFuelUsed += 5;
                    mThrustingRight = true;
                }

                // Lets have some tolerance ..
                float accel = Gdx.input.getAccelerometerX();

                if (Utils.abs(accel)>GamePreferences.mAccelTollerance) {
                    if (accel<0) {
                        addXVelocity(2);
                        mFuelUsed += 2;
                        mThrustingRight = true;
                    }
                    else {
                        addXVelocity(-2);
                        mFuelUsed += 2;
                        mThrustingLeft = true;
                    }
                }
            }
        }
    }

    private void updateSprite(float delta) {

        if (mDamage>=70) {
            mSprite = mDamagedSprite;
            return;
        }

        switch (mState) {
            case INFLIGHT:
            case BOOST:
            case INORBIT:
                mElapsedTime += delta;
                int frameNumber = (int)(mElapsedTime/FRAME_DURATION);
                if (frameNumber%2==0) {
                    mSprite = mBlackSprite;
                }
                else {
                    mSprite = mRedSprite;
                }
                break;

            default:
                break;
        }
    }

    private void checkInUniverse() {
        if (mLocation.y<=0||mLocation.x<=0||mLocation.y>Galaxy.mHeight||mLocation.x>Galaxy.mWidth) {
            mState = ProjectileState.STOPPED;
            GameStatus.setBodyCrashedInto(null);
        }
    }

    public float getFuelRemainingAsPercent() {
        return 100-((mFuelUsed/mTotalFuel)*100);
    }

    /**
     * Set the projectile to orbit a body.
     *
     * @param ascendingBody
     * @param distance
     * @ince 1.0
     */
    public void setInOrbit(AbstractPlanet ascendingBody,float distance) {
        mOrbit.init();
        mOrbit.setAscendingBody(ascendingBody);
        mOrbit.setAngle(getCentreLocation());
        mOrbit.setOrbitRadius(ascendingBody.radius+distance);
        mOrbit.setOrbitSpeed(2,getVelocity());

        if (++mOrbitsThisFlight==6) {
            GameStatus.awardMedal(MedalManager.PLANET_HOPPER);
        }

        mState = ProjectileState.INORBIT;
    }

    /**
     * Set the projectile to land.
     *
     * @param safePlanet
     * @param distance
     */
    public void setLanding(AbstractPlanet safePlanet,float distance) {
        mOrbit.init();
        mOrbit.setAscendingBody(safePlanet);
        mOrbit.setAngle(getCentreLocation());
        mOrbit.setOrbitRadius(safePlanet.radius+distance);
        mOrbit.setOrbitSpeed(4,getVelocity());

        if (getDistance()>=AWARD_MEDAL) {
            GameStatus.awardMedal(MedalManager.STAR_TRECKER);
        }

        mState = ProjectileState.LANDING;
    }

    /**
     * The projectile is in orbit and the user has requested a launch.
     *
     * @since 1.0
     */
    public void boost() {
        // This depends on the mass of the planet...
        mOrbit.inBoost = true;
        mBoostCount = (int)((MathUtils.PI*(mOrbit.getAscendingNode().radius*2))*.50);
        mState = ProjectileState.BOOST;
    }

    public boolean isDocked() {
        return mState==ProjectileState.DOCKED;
    }

    /**
     * Check to see if the projectile is landing.
     *
     * @return true if in landing state.
     * @since 1.0
     */
    public boolean isLanding() {
        return (mState==ProjectileState.LANDING);
    }

    /**
     * Check to see if the projectile is in boost mode.
     *
     * @return true if in boost.
     * @since 1.1
     */
    public boolean isBoost() {
        return (mState==ProjectileState.BOOST);
    }

    /**
     * Check to see if the projectile is dead.
     *
     * @return true if the projectile is dead
     * @since 1.0
     */
    @Override
    public boolean isDead() {
        return mState==ProjectileState.STOPPED;
    }

    /**
     * Set the state of the projectile to dead.
     *
     * @param explosionRequired
     * @since 1.1
     */
    public void setDead(boolean explosionRequired) {
        mState = ProjectileState.STOPPED;
        GamePreferences.incPodDestroyed();
        if (explosionRequired) {
            mExplosion.init(getCentreLocation());
            SoundManager.playExplosion();
        }
    }

    private void setDocked() {
        mState = ProjectileState.DOCKED;
        setLocation(mPlatform.mLocation.x + 24,24);
        mSprite = mDockedSprite;
        mThrustingDown = false;
    }

    /**
     * Set the projectile in flight.
     *
     * @since 1.1
     */
    public void setInFlight() {
        mState = ProjectileState.INFLIGHT;
        mLocation.set(mLocation.x,Platform.PLATFORM_HEIGHT+1);
        mThrustingDown = true;
    }

    /**
     * Check to see if the projectile is in orbit.
     *
     * @return true if the projectile is in orbit.
     * @since 1.0
     */
    public boolean isInOrbit() {
        return (mState==ProjectileState.INORBIT);
    }

    /**
     * Check to see if the projectile is in flight.
     *
     * @return true if the projectile is in flight.
     * @since 1.0
     */
    public boolean isInFlight() {
        return (mState==ProjectileState.INFLIGHT);
    }

    /**
     * Return the current state.
     *
     * @return projectile state
     */
    public ProjectileState getState() {
        return mState;
    }

    @Override
    public boolean intersects(Rectangle rect) {
        return getBounds().overlaps(rect);
    }

    /**
     * Return the graphical bounds of the projectile.
     *
     * @return graphics bounds
     * @since 1.1
     */
    @Override
    public Rectangle getBounds() {
        mBounds.x = mLocation.x;
        mBounds.y = mLocation.y;
        return mBounds;
    }

    @Override
    public void interact(Projectile projectile) {
        // Can't interact with myself
    }

    /**
     * Return the centre point of the projectile.
     *
     * @return centre point.
     * @since 1.1
     */
    public Vector2 getCentreLocation() {
        return mCentrePoint.set(mLocation.x+HALF_PROJECTILE_SIZE,mLocation.y+HALF_PROJECTILE_SIZE);
    }

    /**
     * Return the sprite.
     *
     * @return sprite
     * @since 1.1
     */
    public Sprite getSprite() {
        return mSprite;
    }

    /**
     * Called before the interaction with the bodies.
     *
     * @since 1.1
     */
    public void beginInteract() {
        mOldXOffset = getXVelocity();
    }

    /**
     * Return the amount of 'G' the projectile is being effected by.
     *
     * @return G
     * @since 1.1
     */
    public float getG() {
        return Utils.abs(getXVelocity()-mOldXOffset);
    }

    /**
     * Return the ascending node name.
     *
     * @return ascending node name or null if not in orbit or landing.
     */
    public String getAscendingNodeName() {
        return mOrbit.getAscendingNodeName();
    }

    /**
     * Return the ascending node.
     *
     * @return body or null.
     * @since 1.1
     */
    public Body getAscendingNode() {
        return mOrbit.getAscendingNode();
    }

    @Override
    public String toString() {
        return "Projectile{"+"mVelocity="+getVelocity()+", mLocation="+mLocation+", mState="+mState;
    }

    /**
     * Inflict damage on the projectile.
     *
     * @param amount
     * @since 1.1
     */
    public void inflictDamage(float amount) {
        mDamage += amount;
    }

    /**
     * Return the state of the projectile.
     *
     * @return damage as a range of 0 - 100
     * @since 1.1
     */
    public float getDamage() {
        return mDamage;
    }

    /**
     * Check to see if the projectile is broken.
     *
     * @return true if damaged beyond repair
     */
    public boolean isBroken() {
        return mDamage>=100;
    }

}
