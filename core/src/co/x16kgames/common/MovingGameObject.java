package co.x16kgames.common;

import com.badlogic.gdx.math.Vector2;

public abstract class MovingGameObject extends AbstractGameObject {

    final Vector2 mVelocity;

    public MovingGameObject() {
        mVelocity = new Vector2();
    }

    public void setVelocity(float x,float y) {
        mVelocity.set(x,y);
    }

    public float getXVelocity() {
        return mVelocity.x;
    }

    public void setXVelocity(float x) {
        mVelocity.x = x;
    }

    public void addXVelocity(float delta) {
        mVelocity.x += delta;
    }

    public void addYVelocity(float delta) {
        mVelocity.y += delta;
    }

    public void setYVelocity(float y) {
        mVelocity.y = y;
    }

    public float getYVelocity() {
        return mVelocity.y;
    }

    public Vector2 getVelocity() {
        return mVelocity;
    }
    
    public void setVelocity(Vector2 velocity) {
        mVelocity.set(velocity);
    }
    
    /**
     * Due to the delta, a value of 100 equals about one pixel.
     */
    @Override
    public void update(float delta) {
        mLocation.add(mVelocity.cpy().scl(delta));
    }
}
