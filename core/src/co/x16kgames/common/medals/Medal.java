package co.x16kgames.common.medals;

import co.x16kgames.ar.Assets;
import co.x16kgames.common.Graphics;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;

/**
 * Medals are awarded during the progress of the game.
 *
 * The definition is a comma delimited line of text.
 * 
 * For example <code>1,Private,red,blue,red,blue</code>
 * 
 * @author dhudson - created 24 Sep 2014
 * @since 1.1
 */
public class Medal {

    // Each strip is 8 pixels wide
    private static final int STRIP_WIDTH = 8;
    private static final int STRIP_HEIGHT = 16;

    private static final BitmapFont mFont = Assets.getHUDFont();

    private final Color[] mColour = new Color[4];
    private final String mTitle;
    private final Integer mIdent;
    private float mX;
    private float mY;

    public Medal(String definition) {
        String[] parts = definition.split(",");
        mIdent = Integer.parseInt(parts[0]);
        mTitle = parts[1];

        if (parts.length>2) {
            // Colours defined
            mColour[0] = MedalManager.getColour(parts[2]);
            mColour[1] = MedalManager.getColour(parts[3]);
            mColour[2] = MedalManager.getColour(parts[4]);
            mColour[3] = MedalManager.getColour(parts[5]);
        }
    }

    public Integer getID() {
        return mIdent;
    }

    public void setPosition(float x,float y) {
        mX = x;
        mY = y;
    }

    public void render(Graphics graphics) {
        for (int i = 0;i<4;i++) {
            graphics.setColor(mColour[i]);
            graphics.draw(Assets.getMedalTextureRegion(),mX+(i*STRIP_WIDTH),mY);
        }
        graphics.resetColor();

        mFont.drawMultiLine(graphics.getBatch(),mTitle,mX+32,mY+STRIP_HEIGHT);
    }
    
    public boolean isRank() {
        return mIdent < 20;
    }
    
    public boolean isHonour() {
        return mIdent > 20;
    }

    @Override
    public int hashCode() {
        return mIdent;
    }
    
}
