package co.x16kgames.common;

import com.badlogic.gdx.Gdx;

/**
 * There are lots of different size sizes with different aspect ratios.
 *
 * The default virtual viewport was 480 / 800, and we wan to be able to keep the aspect ratio, so we can extend the
 * virtual viewport to be the best fit for the screen.
 *
 * @author dhudson - 26-Oct-2014
 * @since 1.1
 */
public class VirtualViewport {

    private static final float VIEWPORT_WIDTH = 480f;
    private static final float VIEWPORT_HEIGHT = 800f;

    private float mWidth;
    private float mHeight;

    public VirtualViewport() {
        calculateVirtualViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
    }

    private void calculateVirtualViewport(float width, float height) {

        float virtualAspect = VIEWPORT_WIDTH / VIEWPORT_HEIGHT;
        float aspect = width / height;

        if (aspect > virtualAspect || (Utils.abs((aspect - virtualAspect)) < 0.01f)) {
            mWidth = VIEWPORT_HEIGHT * aspect;
        } else {
            mWidth = VIEWPORT_WIDTH;
        }

        if (aspect > virtualAspect || (Math.abs(aspect - virtualAspect) < 0.01f)) {
            mHeight = VIEWPORT_HEIGHT;
        } else {
            mHeight = VIEWPORT_WIDTH / aspect;
        }
    }

    public float getWidth() {
        return mWidth;
    }

    public float getHeight() {
        return mHeight;
    }
}
