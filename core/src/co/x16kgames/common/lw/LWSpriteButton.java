package co.x16kgames.common.lw;

import co.x16kgames.common.Graphics;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class LWSpriteButton extends LWButton {

    private final Sprite mSprite;
    private boolean isBackgroundRequired;
    private float mAlpha = 1;

    public LWSpriteButton(TextureRegion textureRegion) {
        this(new Sprite(textureRegion));
    }

    public LWSpriteButton(Sprite sprite) {
        mSprite = sprite;
        isBackgroundRequired = true;
    }

    /**
     * Sets the size of the sprite and sets origin to centre.
     *
     * @param width
     * @param height
     * @since 1.1
     */
    public LWSpriteButton setSpriteSize(float width,float height) {
        mSprite.setSize(width,height);
        mSprite.setOrigin(width/2,height/2);
        return this;
    }

    @Override
    public void render(Graphics graphics) {

        if (isVisible) {
            // Background
            if (isBackgroundRequired) {
                super.render(graphics);
            }

            mSprite.setX(mX+5);
            mSprite.setY(mY+5);
            if (mAlpha!=1) {
                graphics.getBatch().setColor(1,1,1,mAlpha);
            }
            mSprite.draw(graphics.getBatch());

            if (mAlpha!=1) {
                graphics.resetColor();
            }
        }
    }

    public Sprite getSprite() {
        return mSprite;
    }

    public void backgroundRequired(boolean required) {
        isBackgroundRequired = required;
    }

    public void setAlpha(float alpha) {
        mAlpha = alpha;
    }
}
