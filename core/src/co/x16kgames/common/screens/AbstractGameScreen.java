package co.x16kgames.common.screens;

import co.x16kgames.common.AbstractGame;
import co.x16kgames.common.Galaxy;
import co.x16kgames.common.Graphics;
import co.x16kgames.common.Utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.input.GestureDetector.GestureListener;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

/**
 * Abstract Game Screen.
 *
 * All Game screens should extend this class.
 *
 * @author dhudson - 06-Sep-2014
 * @since 1.1
 */
public abstract class AbstractGameScreen implements Screen, GestureListener {

    public boolean mPaused;
    public final AbstractGame mGame;
    public final GestureDetector mDectector;
    private final StretchViewport mViewport;
    private final InputMultiplexer mInputMultiplexer;

    // General purpose vector
    public Vector2 mTmpVector2;

    public AbstractGameScreen(final AbstractGame game) {
        mGame = game;
        mPaused = false;
        mDectector = new GestureDetector(this);
        mViewport = new StretchViewport(Galaxy.mWidth, Galaxy.mHeight, createCamera());
        mViewport.update((int) Galaxy.mWidth, (int) Galaxy.mHeight, true);
        mTmpVector2 = new Vector2();
        mInputMultiplexer = new InputMultiplexer(mDectector);
    }

    public Viewport getViewport() {
        return mViewport;
    }

    public void addInputProcessor(InputProcessor inputProcessor) {
        mInputMultiplexer.addProcessor(inputProcessor);
    }

    public void processCheatKeys() {
        if (Gdx.input.isKeyJustPressed(Input.Keys.P)) {
            pause();
        }

        if (Gdx.input.isKeyPressed(Input.Keys.R)) {
            resume();
        }
    }

    @Override
    public void pause() {
        mPaused = true;
    }

    @Override
    public void resume() {
        mPaused = false;
    }

    /**
     * Override this method as its called just before the screen is shown
     */
    public void init() {
        // Check to see if this can be down with show..
    }

    @Override
    public void render(float delta) {
        if(Gdx.input.isKeyJustPressed(Input.Keys.S)) {
            ScreenshotFactory.saveScreenshot();
        }
        
        mViewport.getCamera().update();
        getGraphics().begin(mViewport.getCamera().combined);
        render(getGraphics(), delta);
    }

    @Override
    public void resize(int width, int height) {
        mViewport.update(width, height, true);
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(mInputMultiplexer);
    }

    @Override
    public void hide() {
        Gdx.input.setInputProcessor(null);
    }

    @Override
    public boolean touchDown(float x, float y, int pointer, int button) {
        // This is called before tap and long press
        return false;
    }

    @Override
    public boolean tap(float x, float y, int count, int button) {
        mTmpVector2.set(x, y);
        mTmpVector2 = mViewport.unproject(mTmpVector2);
        return tap(mTmpVector2);
    }

    /**
     * This method is called with an unprojected location.
     *
     * @param position unprojected
     * @return true if has been processed
     * @since 1.1
     */
    public boolean tap(Vector2 position) {
        return false;
    }

    @Override
    public boolean longPress(float x, float y) {
        return false;
    }

    @Override
    public boolean fling(float velocityX, float velocityY, int button) {
        return false;
    }

    @Override
    public boolean pan(float x, float y, float deltaX, float deltaY) {
        return false;
    }

    @Override
    public boolean panStop(float x, float y, int pointer, int button) {
        return false;
    }

    @Override
    public boolean zoom(float initialDistance, float distance) {
        return false;
    }

    @Override
    public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
        return false;
    }

    /**
     * Implementation must create a new Camera
     *
     * @return a newly created camera
     * @since 1.1
     */
    public Camera createCamera() {
        return new OrthographicCamera();
    }

    /**
     * Check to see if the space bar has been pressed.
     *
     * @return true if the space bar has just been pressed.
     * @since 1.1
     */
    public boolean isSpaceJustPressed() {
        return Gdx.input.isKeyJustPressed(Input.Keys.SPACE);
    }

    public boolean isBackJustPressed() {
        return Gdx.input.isKeyJustPressed(Keys.BACK);
    }

    public float getViewportWidth() {
        return mViewport.getWorldWidth();
    }

    public float getViewportHeight() {
        return mViewport.getWorldHeight();
    }

    public float getHalfViewportWidth() {
        return getViewportWidth() / 2;
    }

    public float getHalfViewportHeight() {
        return getViewportHeight() / 2;
    }

    public float getScreenWidth() {
        return Gdx.graphics.getWidth();
    }

    public float getScreenHeight() {
        return Gdx.graphics.getHeight();
    }

    public ScreenManager getScreenManager() {
        return mGame.getScreenManager();
    }

    public Graphics getGraphics() {
        return mGame.getGraphics();
    }

    /**
     * Camera updated and graphics started.
     *
     * Remember to call graphics.end() before this method ends
     *
     * @param graphics
     * @param delta
     * @since 1.1
     */
    public abstract void render(Graphics graphics, float delta);

    /**
     * Called once upon after the constructor.
     *
     * With this method you should have full access to camera and viewports.
     *
     * @since 1.1
     */
    public abstract void create();
}
