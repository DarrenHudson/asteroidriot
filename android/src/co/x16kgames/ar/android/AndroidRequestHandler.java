package co.x16kgames.ar.android;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.graphics.Color;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import co.x16kgames.common.PlatformRequestHandler;
import co.x16kgames.common.Utils;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

/**
 * Android implementation of the Request Handler
 *
 * @author dhudson - created 15 Oct 2014
 * @since 1.1
 */
public class AndroidRequestHandler implements PlatformRequestHandler {

    /**
     * Test ID for the NEXUS
     */
    public final String NEXUS = "078720DD886542A8AB0B544166F5EB";

    // Interstitial
    public static final String GAME_OVER_ID = "ca-app-pub-6142084001875032/1528889704";

    // Banner
    public static final String HOME_SCREEN_ID = "ca-app-pub-6142084001875032/9052156506";

    private final AdRequest mAdRequest;
    private AdView mBannerView;
    private InterstitialAd mInterstitialAd;

    private boolean isBannerLoaded;
    private final Context mContext;
    private final AdvertMessageHandler mAdvertHandler;
    private boolean isShowing;

    public AndroidRequestHandler(Context context) {
        mContext = context;
        mAdvertHandler = new AdvertMessageHandler(this);

        createBannerAd(context);
        createInterstitialAd(context);
        mAdRequest = new AdRequest.Builder().addTestDevice(NEXUS).build();

        // Start off with Banners showing
        isShowing = true;
    }

    @Override
    public void loadAds() {
        mBannerView.loadAd(mAdRequest);
        loadInterstitial();
    }

    private void createBannerAd(Context context) {
        // Create Banner Ad
        mBannerView = new AdView(context);
        mBannerView.setAdSize(AdSize.SMART_BANNER);
        mBannerView.setAdUnitId(HOME_SCREEN_ID);
        mBannerView.setId(12345); // this is an arbitrary id, allows for relative positioning in createGameView()

        RelativeLayout.LayoutParams params =
            new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM,RelativeLayout.TRUE);
        params.addRule(RelativeLayout.CENTER_HORIZONTAL,RelativeLayout.TRUE);
        mBannerView.setLayoutParams(params);
        mBannerView.setBackgroundColor(Color.BLACK);

        // Set the view to invisible until its loaded.
        mBannerView.setVisibility(View.GONE);
        mBannerView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                Utils.debug("Ad Loaded .... "+mBannerView.getAdSize().getHeightInPixels(mContext));
                isBannerLoaded = true;

                // Might have loaded during the play screen and we don't want to show it then.
                if (isShowing) {
                    showBannerAd(true);
                }
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                // If the ad failed to load, then there is no point allocating the space for it
                // Airplane mode is a good way of testing for this feature.
                Utils.debug("Failed to load Advert .. "+errorCode);
                isBannerLoaded = false;
            }
        });
    }

    AdView getBannerAddView() {
        return mBannerView;
    }

    InterstitialAd getInterstitialAd() {
        return mInterstitialAd;
    }

    private void createInterstitialAd(Context context) {
        // Create Interstitial Ad
        mInterstitialAd = new InterstitialAd(context);
        mInterstitialAd.setAdUnitId(GAME_OVER_ID);
        mInterstitialAd.setAdListener(new AdListener() {
            /**
             * Once an Interstitial has been closed, need to an another one
             */
            @Override
            public void onAdClosed() {
                loadInterstitial();
            }
        });
    }

    @Override
    public void showInterstitial() {
        mAdvertHandler.showInterstitial();
    }

    @Override
    public void showBannerAd(boolean show) {
        isShowing = show;
        if (show) {
            if (isBannerLoaded) {
                mAdvertHandler.showBannerAd();
            }
            else {
                mAdvertHandler.loadAds();
            }
        }
        else {
            mAdvertHandler.hideBannerAd();
        }
    }

    private void loadInterstitial() {
        mInterstitialAd.loadAd(mAdRequest);
    }

    @Override
    public void onDestroy() {
        mBannerView.destroy();
    }

    @Override
    public void onResume() {
        mBannerView.resume();
    }

    @Override
    public void onPause() {
        mBannerView.pause();
    }

    @Override
    public String getVersion() {
        try {
            PackageInfo pInfo = mContext.getPackageManager().getPackageInfo(mContext.getPackageName(),0);
            return pInfo.versionName;
        }
        catch (Throwable t) {
            return "Unknown";
        }
    }
}
