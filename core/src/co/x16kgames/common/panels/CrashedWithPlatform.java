package co.x16kgames.common.panels;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.scenes.scene2d.utils.Align;

/**
 * The projectile has hit the platform.
 *
 * Total wipe out.
 *
 * @author dhudson - 17-Sep-2014
 * @since 1.1
 */
public class CrashedWithPlatform extends AbstractPanel {

    public CrashedWithPlatform(Camera camera) {
        super(camera);
        mRoot.add(createLabel("Seppuku!")).pad(10).align(Align.center).row();
    }

    @Override
    public void populate() {
        packAndCentre();
    }

}
