package co.x16kgames.ar.android;

import co.x16kgames.common.Utils;
import android.os.Handler;
import android.os.Message;
import android.view.View;

/**
 * Make sure that this is done on the UI thread.
 *
 * @author dhudson - created 4 Nov 2014
 * @since 5.0
 */
public class AdvertMessageHandler extends Handler {

    private final AndroidRequestHandler mRequestHandler;

    private final int SHOW_BANNER = 0;
    private final int HIDE_BANNER = 1;
    private final int SHOW_INTERSTITIAL = 2;
    private final int LOAD_ADS = 3;

    public AdvertMessageHandler(AndroidRequestHandler requestHandler) {
        mRequestHandler = requestHandler;
    }

    @Override
    public void handleMessage(Message msg) {
        switch (msg.what) {
            case SHOW_BANNER:
                Utils.debug("Show Banner");
                // This should only be called if the banner has been loaded.
                mRequestHandler.getBannerAddView().setVisibility(View.VISIBLE);
                break;

            case HIDE_BANNER:
                mRequestHandler.getBannerAddView().setVisibility(View.GONE);
                break;

            case SHOW_INTERSTITIAL:
                if(mRequestHandler.getInterstitialAd().isLoaded()) {
                    mRequestHandler.getInterstitialAd().show();
                }
                break;

            case LOAD_ADS:
                mRequestHandler.loadAds();
                break;
        }
    }

    public void showBannerAd() {
        sendEmptyMessage(SHOW_BANNER);
    }

    public void hideBannerAd() {
        sendEmptyMessage(HIDE_BANNER);
    }

    public void showInterstitial() {
        sendEmptyMessage(SHOW_INTERSTITIAL);
    }

    public void loadAds() {
        sendEmptyMessage(LOAD_ADS);
    }
}
