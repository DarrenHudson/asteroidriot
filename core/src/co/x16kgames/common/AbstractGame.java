package co.x16kgames.common;

import co.x16kgames.common.screens.ScreenManager;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;

/**
 * Common aspects between the different games.
 *
 * @author dhudson - 29-Oct-2014
 * @since 4.0
 */
public abstract class AbstractGame extends Game {

    public Graphics mGraphics;
    public ScreenManager mScreenManager;
    public final PlatformRequestHandler mRequestHandler;

    public AbstractGame(PlatformRequestHandler requestHandler) {
        mRequestHandler = requestHandler;
        mScreenManager = new ScreenManager(this);
    }

    @Override
    public void create() {
        // This needs to be done here else there will be a {@code java.lang.UnsatisfiedLinkError} in desktop.
        mGraphics = new Graphics();
        // Catch the back key
        Gdx.input.setCatchBackKey(true);
    }

    public Graphics getGraphics() {
        return mGraphics;
    }

    public PlatformRequestHandler getRequestHandler() {
        return mRequestHandler;
    }

    public ScreenManager getScreenManager() {
        return mScreenManager;
    }

    @Override
    public void dispose() {
        // dispose of all the native resources
        mGraphics.dispose();
        mScreenManager.dispose();
    }
}
