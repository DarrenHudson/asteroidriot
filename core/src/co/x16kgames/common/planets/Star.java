package co.x16kgames.common.planets;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import co.x16kgames.ar.Assets;
import co.x16kgames.common.Drawable;
import co.x16kgames.common.Galaxy;
import co.x16kgames.common.Graphics;
import co.x16kgames.common.Utils;
import co.x16kgames.common.projectile.Projectile;

/**
 * Class to represent a star.
 *
 * @author dhudson - 29-Aug-2014
 * @since 1.0
 */
public final class Star extends Rectangle implements Drawable {

    private static final int STAR_SIZE = 2;
    private static final int FLICKER_MAX = 500;

    private int mFlicker;
    private int mCount;
    private int mFlickerLength;
    private int mFlickerCount;

    public Star() {
        // Set up the rect ...
        width = STAR_SIZE;
        height = STAR_SIZE;
        init();
    }

    @Override
    public void init() {
        mCount = 0;
        mFlicker = MathUtils.random(11, FLICKER_MAX);
        mFlickerLength = MathUtils.random(1, 10);

        Utils.setRandomPoint(Galaxy.mGalaxyBounds, this);
    }

    @Override
    public void render(Graphics graphics) {
        if (mFlickerCount > 0) {
            graphics.getBatch().setColor(Color.RED);
            graphics.getBatch().draw(Assets.getBlockTextureRegion(), x, y);
            graphics.getBatch().setColor(Color.WHITE);
        } else {
            graphics.getBatch().draw(Assets.getBlockTextureRegion(), x, y);
        }
    }

    @Override
    public boolean intersects(Rectangle rect) {
        return false;
    }

    @Override
    public void update(float delta) {
        if (mCount++ > FLICKER_MAX) {
            mCount = 0;
        }

        if (mCount == mFlicker) {
            // Lets start to flicker..
            mFlickerCount = mFlickerLength;
        }

        if (mFlickerCount > 0) {
            mFlickerCount--;
        }
    }

    @Override
    public void interact(Projectile projectile) {
    }

    @Override
    public boolean isDead() {
        return false;
    }

    @Override
    public void setLocation(float x, float y) {
    }


    @Override
    public Rectangle getBounds() {
        return this;
    }

}
