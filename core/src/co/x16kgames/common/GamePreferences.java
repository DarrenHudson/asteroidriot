package co.x16kgames.common;

import co.x16kgames.common.medals.Medal;
import co.x16kgames.common.medals.MedalManager;

import java.util.HashSet;
import java.util.Set;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.math.MathUtils;

/**
 * Wrapper for the game preferences statistics and medals.
 *
 * @author dhudson - created 22 Sep 2014
 * @since 1.1
 */
public class GamePreferences {

    private static final String HIGHEST_LEVEL_KEY = "hl-ar";
    private static final String NO_OF_PLAYS_KEY = "np-ar";
    private static final String NO_OF_ASTEROIDS_DESTROYED_KEY = "noad";
    private static final String NO_OF_PODS_DESTROYED_KEY = "nopd";
    private static final String NO_OF_LEVELS_COMPLETED_KEY = "nolc";
    private static final String NO_OF_LEVELS_PLAYED_KEY = "nolp";
    private static final String MEDALS_KEY = "medals";
    private static final String SOUND_REQUIRED_KEY = "sound";
    private static final String MUSIC_REQUIRED_KEY = "music";
    private static final String SOUND_VOLUME_KEY = "sound-volume";
    private static final String MUSIC_VOLUME_KEY = "music-volume";
    private static final String ACCELEROMETER_TOLLERANCE_KEY = "acc-tol";
    private static final String ACCELEROMETER_REQUIRED_KEY = "acc-req";
    private static final String LEFT_HANDED_KEY = "left-handed";

    private final static Preferences mPrefs = Gdx.app.getPreferences("AsteroidRiot");

    private static int mHighestLevel;
    private static int mNoPlays;
    private static int mNoOfAsteroidsDestroyed;
    private static int mNoOfPodsDestroyed;
    private static int mNoOfLevelsCompleted;
    private static int mNoOfLevelsPlayed;

    private final static HashSet<Medal> mMedals = new HashSet<Medal>(5);

    public static boolean isSoundRequired;
    public static boolean isMusicRequired;
    public static float mSoundVolume;
    public static float mMusicVolume;

    public static float mAccelTollerance;
    public static boolean isAccelerometerRequired;
    public static boolean isLeftHanded;

    private static final StringBuilder mBuilder = new StringBuilder();

    private static final float ACCEL_TOLLERANCE_DEFAULT = 2.0f;
    private static final float MUSIC_VOLUME_DEFAULT = 0.5f;
    private static final float SOUND_VOLUME_DEFAULT = 0.5f;
    private static final boolean IS_LEFT_HANED_DEFAUL = false;

    public static void load() {
        mHighestLevel = mPrefs.getInteger(HIGHEST_LEVEL_KEY, 0);
        mNoPlays = mPrefs.getInteger(NO_OF_PLAYS_KEY, 0);
        mNoOfAsteroidsDestroyed = mPrefs.getInteger(NO_OF_ASTEROIDS_DESTROYED_KEY, 0);
        mNoOfPodsDestroyed = mPrefs.getInteger(NO_OF_PODS_DESTROYED_KEY, 0);
        mNoOfLevelsCompleted = mPrefs.getInteger(NO_OF_LEVELS_COMPLETED_KEY, 0);
        mNoOfLevelsPlayed = mPrefs.getInteger(NO_OF_LEVELS_PLAYED_KEY, 0);
        isSoundRequired = mPrefs.getBoolean(SOUND_REQUIRED_KEY, true);
        isMusicRequired = mPrefs.getBoolean(MUSIC_REQUIRED_KEY, true);
        mSoundVolume = MathUtils.clamp(mPrefs.getFloat(SOUND_VOLUME_KEY, SOUND_VOLUME_DEFAULT), 0.0f, 1.0f);
        mMusicVolume = MathUtils.clamp(mPrefs.getFloat(MUSIC_VOLUME_KEY, MUSIC_VOLUME_DEFAULT), 0.0f, 1.0f);
        mAccelTollerance
                = MathUtils.clamp(mPrefs.getFloat(ACCELEROMETER_TOLLERANCE_KEY, ACCEL_TOLLERANCE_DEFAULT), 0.0f, 10f);
        isAccelerometerRequired = mPrefs.getBoolean(ACCELEROMETER_REQUIRED_KEY, true);
        isLeftHanded = mPrefs.getBoolean(LEFT_HANDED_KEY, IS_LEFT_HANED_DEFAUL);

        loadMedals();
    }

    private static void loadMedals() {

        String medalList = mPrefs.getString(MEDALS_KEY, "");
        if (medalList == null || medalList.equals("")) {
            // No medals yet.
            return;
        }

        String[] medals = medalList.split(",");

        for (int i = 0; i < medals.length; i++) {
            mMedals.add(MedalManager.getMedal(Integer.parseInt(medals[i])));
        }
    }

    private static void saveMedals() {
        if (mMedals.isEmpty()) {
            mPrefs.putString(MEDALS_KEY, "");
        } else {
            StringBuilder builder = getBuilder();
            for (Medal medal : mMedals) {
                builder.append(medal.getID());
                builder.append(",");
            }

            // Remove the last ,
            builder.setLength(builder.length() - 1);
            mPrefs.putString(MEDALS_KEY, builder.toString());
        }
    }

    public static boolean hasMedal(int ID) {
        for (Medal medal : mMedals) {
            if (medal.getID() == ID) {
                return true;
            }
        }

        return false;
    }

    public static void awardMedal(Medal medal) {
        mMedals.add(medal);
    }

    public static Set<Medal> getMedals() {
        return mMedals;
    }

    private static StringBuilder getBuilder() {
        mBuilder.setLength(0);
        return mBuilder;
    }

    public static void save() {
        mPrefs.putInteger(HIGHEST_LEVEL_KEY, mHighestLevel);
        mPrefs.putInteger(NO_OF_PLAYS_KEY, mNoPlays);
        mPrefs.putInteger(NO_OF_ASTEROIDS_DESTROYED_KEY, mNoOfAsteroidsDestroyed);
        mPrefs.putInteger(NO_OF_PODS_DESTROYED_KEY, mNoOfPodsDestroyed);
        mPrefs.putInteger(NO_OF_LEVELS_COMPLETED_KEY, mNoOfLevelsCompleted);
        mPrefs.putInteger(NO_OF_LEVELS_PLAYED_KEY, mNoOfLevelsPlayed);

        mPrefs.putBoolean(SOUND_REQUIRED_KEY, isSoundRequired);
        mPrefs.putBoolean(MUSIC_REQUIRED_KEY, isMusicRequired);
        mPrefs.putFloat(SOUND_VOLUME_KEY, mSoundVolume);
        mPrefs.putFloat(MUSIC_VOLUME_KEY, mMusicVolume);

        mPrefs.putBoolean(ACCELEROMETER_REQUIRED_KEY, isAccelerometerRequired);
        mPrefs.putFloat(ACCELEROMETER_TOLLERANCE_KEY, mAccelTollerance);
        mPrefs.putBoolean(LEFT_HANDED_KEY, isLeftHanded);

        saveMedals();

        mPrefs.flush();
    }

    public static void resetInput() {
        isAccelerometerRequired = true;
        mAccelTollerance = ACCEL_TOLLERANCE_DEFAULT;
        isLeftHanded = false;
    }

    public static void resetAudio() {
        isMusicRequired = true;
        isSoundRequired = true;
        mMusicVolume = MUSIC_VOLUME_DEFAULT;
        mSoundVolume = SOUND_VOLUME_DEFAULT;
    }

    public static void resetStats() {
        mHighestLevel = 0;
        mNoPlays = 0;
        mNoOfAsteroidsDestroyed = 0;
        mNoOfPodsDestroyed = 0;
        mNoOfLevelsCompleted = 0;
        mNoOfLevelsPlayed = 0;
    }

    public static void resetMedals() {
        mMedals.clear();
    }

    public static boolean setHighestLevel(int level) {
        if (level > mHighestLevel) {
            mHighestLevel = level;
            return true;
        }

        return false;
    }

    public static int getHighestLevel() {
        return mHighestLevel;
    }

    public static void incPlays() {
        mNoPlays++;
    }

    public static void incLevelsComplete() {
        mNoOfLevelsCompleted++;
    }

    public static void incLevelsPlayed() {
        mNoOfLevelsPlayed++;
    }

    public static int incAsteroidsDestroyed() {
        mNoOfAsteroidsDestroyed++;
        return mNoOfAsteroidsDestroyed;
    }

    public static void incPodDestroyed() {
        mNoOfPodsDestroyed++;
    }

    public static int getGamesPlayed() {
        return mNoPlays;
    }

    public static void resetAll() {
        resetAudio();
        resetInput();
        resetStats();
        resetMedals();
    }
}
