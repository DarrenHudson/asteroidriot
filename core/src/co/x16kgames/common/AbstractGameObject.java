package co.x16kgames.common;

import co.x16kgames.common.projectile.Projectile;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * Common Attributes of a Game Object.
 * 
 * Actor in Libgdx speak.
 *
 * @author dhudson - created 26 Sep 2014
 * @since 1.1
 */
public abstract class AbstractGameObject implements Drawable {

    /**
     * Position on in game world.
     */
    public final Vector2 mLocation;
    boolean isDead;

    public AbstractGameObject() {
        mLocation = new Vector2();
    }

    public void setLocation(Vector2 location) {
        mLocation.set(location);
    }

    public void setLocation(float x,float y) {
        mLocation.x = x;
        mLocation.y = y;
    }

    public Vector2 getLocation() {
        return mLocation;
    }

    public boolean intersects(Rectangle rect) {
        return getBounds().overlaps(rect);
    }

    public boolean isDead() {
        return isDead;
    }

    /**
     * Configures a square sprite with its origin in the centre.
     * 
     * @param sprite
     * @param size
     * @since 1.1
     */
    public void configureSprite(Sprite sprite,float size) {
        configureSprite(sprite,size,size);
    }

    /**
     * Configures a rectangular sprite with a centre origin.
     * 
     * @param sprite
     * @param width
     * @param height
     * @since 5.1
     */
    public void configureSprite(Sprite sprite,float width, float height) {
        sprite.setSize(width,height);
        sprite.setOriginCenter();   
    }
    
    /**
     * Interact with the projectile.
     *
     * @param projectile current projectile
     * @since 1.0
     */
    public abstract void interact(Projectile projectile);

    /**
     * Called after render.
     * 
     * @param delta time since last called
     *
     * @since 1.0
     */
    public abstract void update(float delta);

    public abstract void render(Graphics graphics);

    public abstract Rectangle getBounds();

    public abstract void init();

}
