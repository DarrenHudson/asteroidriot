package co.x16kgames.common.planets;

import co.x16kgames.common.Graphics;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;

/**
 * Abstract class for orbiting body
 *
 * @author dhudson - created 16 Feb 2014
 * @since 1.0
 */
public abstract class OrbitingBody extends Body {

    /**
     * Speed of travel
     */
    private float mVelocity;

    /**
     * Parent is the wrong word here
     */
    private final Body mParent;

    /**
     * Size of the orbit
     */
    private float mOrbit;

    /**
     * The inclination angle from the x axis of the orbit, in radians
     */
    private float theInclination;

    // Cache these values
    float mSinVal;
    float mCosVal;

    /**
     * Constructor.
     *
     * @param parent Ascending node
     * @since 1.0
     */
    public OrbitingBody(Sprite sprite,Body parent) {
        super(sprite);
        mParent = parent;
    }

    /**
     * Get the body that this orbits.
     *
     * @return the body that this orbits
     * @since 1.0
     */
    public Body getParent() {
        return mParent;
    }

    /**
     * Set the velocity
     *
     * @param vel to set
     * @since 1.0
     */
    public void setVelocity(float vel) {
        mVelocity = vel;
    }

    /**
     * Return the Orbiting body
     *
     * @return the current velocity of the orbiting body
     * @since 1.0
     */
    public float getVelocity() {
        return mVelocity;
    }

    /**
     * Set the size of the orbit
     *
     * @param orbit
     * @since 1.0
     */
    public void setOrbit(float orbit) {
        mOrbit = orbit;
    }

    /**
     * Return the size of the orbit.
     *
     * @return the size of the orbit
     * @since 1.0
     */
    public float getOrbit() {
        return mOrbit;
    }

    /**
     * Get the inclination angle.
     *
     * @return the inclination angle
     * @since 1.0
     */
    public float getInclination() {
        return theInclination;
    }

    /**
     * Set the inclination of the orbit.
     *
     * Also set cos and sin values
     * 
     * @param radians to offset
     * @since 1.0
     */
    public void setInclination(float radians) {
        theInclination = radians;
        mSinVal = MathUtils.sin(radians);
        mCosVal = MathUtils.cos(radians);
    }

    /**
     * If the moon is behind the planet, draw it.
     *
     * @param graphics to use
     * @since 1.0
     */
    public abstract void preParentRender(Graphics graphics);

    /**
     * If the moon is in front of the planet, draw it.
     *
     * @param graphics to use
     * @since 1.0
     */
    public abstract void postParentRender(Graphics graphics);

    @Override
    public void changeLocation(Rectangle bounds) {
    }
}
