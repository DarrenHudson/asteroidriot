package co.x16kgames.common.panels;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import co.x16kgames.ar.Assets;

/**
 * Base for all status panels.
 *
 * @author dhudson - 16-Sep-2014
 * @since 1.1
 */
public abstract class AbstractPanel {

    final Table mRoot;
    final Camera mCamera;
    
    public AbstractPanel(Camera camera) {
        mCamera = camera;
        mRoot = new Table(Assets.getDefaultSkin());
        // Slightly opaque
        mRoot.setColor(1, 1, 1, 0.85f);
        mRoot.setTouchable(Touchable.disabled);

        // Ninepatch from the Skin
        mRoot.background("default-pane");
        mRoot.setVisible(false);
    }

    public Table getRoot() {
        return mRoot;
    }

    public void setVisible(boolean visable) {
        mRoot.setVisible(visable);
    }

    public boolean isVisible() {
        return mRoot.isVisible();
    }

    Label createLabel(CharSequence text) {
        return new Label(text, Assets.getDefaultSkin());
    }

    Label createLabel() {
        return createLabel("");
    }

    void centrePanel() {
        mRoot.setX((mCamera.viewportWidth / 2) - (mRoot.getWidth() / 2));
        mRoot.setY((mCamera.viewportHeight / 2) - (mRoot.getHeight() / 2));
    }

    public void packAndCentre() {
        mRoot.pack();
        centrePanel();
    }
        
    public abstract void populate();
}
