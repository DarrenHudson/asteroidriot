package co.x16kgames.common;

import co.x16kgames.common.medals.Medal;
import co.x16kgames.common.medals.MedalManager;
import co.x16kgames.common.planets.Body;

import java.util.HashSet;

/**
 * Horrible static class to hold the game status.
 *
 * @author dhudson - 10-Sep-2014
 * @since 1.1
 */
public class GameStatus {

    public enum Status {

        /**
         * Showing the user where the safe planet is.
         */
        PANNING_TO_PLANET,
        /**
         * Awaiting user input.
         */
        WAITING_TO_LAUNCH,
        /**
         * We are away
         */
        IN_FLIGHT,
        /**
         * Landed safely
         */
        LEVEL_COMPLETE,
        /**
         * Projectile crashed
         */
        PROJECTILE_CRASHED,
        /**
         * Hmm ...
         */
        GAME_OVER,
        /**
         * Info Panel
         */
        INFO_PANEL
    }

    private static Status mStatus;

    private static Body mCrashedInto;

    // Number of projectiles left
    public static int mLevel;
    private static int mRemainingProjectiles;

    private static final float PAUSE_TIME = 3;
    public static float mWindowDelay;

    private static Status tmpStatus;

    // Has projectile crashed into platform
    private static boolean mSeppuku;

    private static short mGravityMasterCount;
    
    private final static HashSet<Medal> mAwardedMedals = new HashSet<Medal>(10);

    public static void init() {
        mStatus = Status.PANNING_TO_PLANET;
        mCrashedInto = null;
        // Including of zero
        mRemainingProjectiles = 4;
        mLevel = 1;
        mWindowDelay = PAUSE_TIME;
        mSeppuku = false;
        mAwardedMedals.clear();
        mGravityMasterCount = 0;
    }

    public static void resetLevel() {
        mLevel = 1;
    }

    public static Status getStatus() {
        return mStatus;
    }

    /**
     * Should not use this, in practice, its a short cut to change state.
     *
     * @param status
     */
    public static void setStatus(Status status) {
        mStatus = status;
    }

    public static void setCrashedIntoPlatform() {
        mSeppuku = true;
        crashed();
    }

    public static boolean crashedIntoPlatform() {
        return mSeppuku;
    }

    public static void setBodyCrashedInto(Body body) {
        mCrashedInto = body;
        crashed();
    }

    private static void crashed() {
        mStatus = Status.PROJECTILE_CRASHED;
        mWindowDelay = PAUSE_TIME;
        mRemainingProjectiles--;
    }

    public static Body getCrashedInto() {
        return mCrashedInto;
    }

    public static boolean isWaitingToLaunch() {
        return (mStatus == Status.WAITING_TO_LAUNCH);
    }

    public static void reduceWindowTime(float delta) {
        mWindowDelay -= delta;
    }

    public static boolean isInfoPanel() {
        return (mStatus == Status.INFO_PANEL);
    }

    public static void setInfoPanel() {
        tmpStatus = mStatus;
        mWindowDelay = PAUSE_TIME;
        mStatus = Status.INFO_PANEL;
    }

    public static void resetInfoPanel() {
        mStatus = tmpStatus;
    }

    public static boolean isPaused() {
        return mWindowDelay > 0;
    }

    public static void setLevelComplete() {
        mWindowDelay = PAUSE_TIME;
        mStatus = Status.LEVEL_COMPLETE;
        mLevel++;
    }

    public static void setGameOver() {
        mStatus = Status.GAME_OVER;
    }

    public static boolean isProjectileCrashed() {
        return (mStatus == Status.PROJECTILE_CRASHED);
    }

    public static int getRemainingProjectiles() {
        return mRemainingProjectiles;
    }

    public static void incrementRemainingProjectiles() {
        mRemainingProjectiles++;
    }
    
    public static boolean hasNewMedals() {
        return !mAwardedMedals.isEmpty();
    }

    public static void awardMedal(int ID) {
        // Make sure that we don't already have it ..
        if (!GamePreferences.hasMedal(ID)) {
            Medal medal = MedalManager.getMedal(ID);
            mAwardedMedals.add(medal);
            GamePreferences.awardMedal(medal);
        }
    }

    public static HashSet<Medal> getNewMedals() {
        return mAwardedMedals;
    }
    
    /**
     * Increment the count and check for reward.
     * 
     * @return true if the medal should be awarded. 
     */
    public static boolean increaseGravityMaster() {
        mGravityMasterCount++;
        return mGravityMasterCount == 6;
    }
}
