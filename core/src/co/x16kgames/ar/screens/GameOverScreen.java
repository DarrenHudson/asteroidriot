package co.x16kgames.ar.screens;

import co.x16kgames.common.screens.AbstractGameScreen;
import co.x16kgames.ar.Assets;
import co.x16kgames.common.GamePreferences;
import co.x16kgames.common.GameStatus;
import co.x16kgames.common.Graphics;
import co.x16kgames.common.StarField;
import co.x16kgames.common.explosion.Firework;
import co.x16kgames.common.explosion.Fireworks;
import co.x16kgames.common.medals.Medal;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import co.x16kgames.common.AbstractGame;

/**
 * Show the Game Over.
 *
 * @author dhudson - 10-Sep-2014
 * @since 1.1
 */
public class GameOverScreen extends AbstractGameScreen {

    private boolean mNewHighScore;
    private final Fireworks mFireworks;
    private final StarField mStarField;

    public GameOverScreen(AbstractGame game) {
        super(game);
        mStarField = new StarField();
        mFireworks = new Fireworks();
        mTmpVector2.set(100, 100);
        mFireworks.addFirework(new Firework(100, 400, Color.YELLOW, 30));
        mFireworks.addFirework(new Firework(200, 200, Color.GREEN, 90));
        mFireworks.addFirework(new Firework(400, 700, Color.RED, 120));
    }

    @Override
    public void init() {

        mStarField.init();
        mFireworks.setDead();

        GamePreferences.incPlays();
        mNewHighScore = GamePreferences.setHighestLevel(GameStatus.mLevel);

        // Check for rank upgrade
        int medal = GamePreferences.getGamesPlayed() / 10;
        if (medal != 0) {
            GameStatus.awardMedal(medal);
        }

        if (mNewHighScore || GameStatus.hasNewMedals()) {
            mFireworks.init();
        }

        // Check for High Score
        GamePreferences.save();
    }

    @Override
    public void render(Graphics graphics, float delta) {

        mStarField.render(graphics);

        Assets.getLargeFont().drawMultiLine(graphics.getBatch(), "Game Over!", getHalfViewportWidth(),
                getHalfViewportHeight() + 50,
                10, BitmapFont.HAlignment.CENTER);

        if (mNewHighScore) {
            Assets.getMediumFont().drawMultiLine(graphics.getBatch(),
                    "Congrats!\nYou reached a new\nhighest level of " + GameStatus.mLevel,
                    getHalfViewportWidth(), getViewportHeight() - 100,
                    10, BitmapFont.HAlignment.CENTER);
        } else {
            Assets.getMediumFont().drawMultiLine(graphics.getBatch(), "You reached level " + GameStatus.mLevel,
                    getHalfViewportWidth(), getViewportHeight() - 100,
                    10, BitmapFont.HAlignment.CENTER);
        }

        if (GameStatus.hasNewMedals()) {
            Assets.getMediumFont().drawMultiLine(graphics.getBatch(), "New Medals ",
                    getHalfViewportWidth(), 200,
                    10, BitmapFont.HAlignment.CENTER);

            int y = 150;
            for (Medal medal : GameStatus.getNewMedals()) {
                medal.setPosition(getHalfViewportWidth() - 50, y);
                medal.render(graphics);
                y -= 20;
            }
        }

        Assets.getMediumFont().drawMultiLine(graphics.getBatch(),"Tap to continue",getHalfViewportWidth(),50,10,BitmapFont.HAlignment.CENTER);
        
        mFireworks.render(graphics);

        getGraphics().end();

        processCheatKeys();
        
        if (isSpaceJustPressed() || isBackJustPressed()) {
            nextScreen();
        }

        if(!mPaused) {
            mStarField.update(delta);
            mFireworks.update(delta);
        }
    }

    @Override
    public void dispose() {
    }

    @Override
    public boolean tap(float x, float y, int count, int button) {
        nextScreen();
        return true;
    }

    private void nextScreen() {
        mFireworks.setDead();
        getScreenManager().showMenuScreen();
    }

    @Override
    public void create() {
    }
}
