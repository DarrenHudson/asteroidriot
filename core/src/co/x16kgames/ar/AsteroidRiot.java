package co.x16kgames.ar;

import co.x16kgames.common.AbstractGame;
import co.x16kgames.common.GamePreferences;
import co.x16kgames.common.Galaxy;
import co.x16kgames.common.Utils;
import co.x16kgames.common.PlatformRequestHandler;
import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import co.x16kgames.common.medals.MedalManager;
import java.io.IOException;

public class AsteroidRiot extends AbstractGame {

    // Set to false this for launch
    public static final boolean isDebugging = false;

    public AsteroidRiot(PlatformRequestHandler handler) {
        super(handler);
    }

    @Override
    public void create() {
        super.create();

        try {

            if (isDebugging) {
                Gdx.app.setLogLevel(Application.LOG_DEBUG);
            }
            else {
                Gdx.app.setLogLevel(Application.LOG_NONE);
            }

            // Load all of the assets.
            Assets.load();

            // Load medals, this needs to be done before preferences
            MedalManager.load();

            // Load the preferences
            GamePreferences.load();

            new Galaxy();

            Galaxy.init();

            if (!Gdx.app.getType().equals(Application.ApplicationType.iOS)) {
                getScreenManager().showSplashScreen();
            }
            else {
                // Robovm will display a splash screen.
                getScreenManager().showMenuScreen();
            }

        }
        catch (IOException ex) {
            Utils.debug("Unable to load Assets ["+ex.getMessage()+"]");
        }
    }

    @Override
    public void dispose() {
        super.dispose();
        // dispose of all the native resources
        Assets.dispose();
    }
}
