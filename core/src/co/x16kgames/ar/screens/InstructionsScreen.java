package co.x16kgames.ar.screens;

import co.x16kgames.ar.Assets;
import co.x16kgames.common.AbstractGame;
import co.x16kgames.common.Graphics;
import co.x16kgames.common.planets.Asteroid;
import co.x16kgames.common.planets.SafePlanet;
import co.x16kgames.common.screens.AbstractGameScreen;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

/**
 * Introduce instruction screen.
 *
 * @author dhudson - created 27 Dec 2014
 * @since 5.3
 */
public class InstructionsScreen extends AbstractGameScreen {

    private final BitmapFont mMediumFont;
    private final BitmapFont mSmallFont;

    private final Sprite mTablet;
    private float mRotate;
    private SafePlanet mSafePlanet;
    private final Sprite mLaunchPod;
    private final Sprite mProjectileSprite;
    private Asteroid mAsteroid;
    
    public InstructionsScreen(AbstractGame game) {
        super(game);

        mSmallFont = Assets.getSmallFont();
        mMediumFont = Assets.getMediumFont();

        mTablet = Assets.getTabletSprite();
        mTablet.setSize(64,64);
        mTablet.setOriginCenter();
        mTablet.setX(20);
        mTablet.setY(getViewportHeight()-420);

        mLaunchPod = new Sprite(Assets.getLaunchSprite());
        // Maintain aspect ratio
        mLaunchPod.setSize(64,54);
        mLaunchPod.setOriginCenter();
        mLaunchPod.setX(20);
        mLaunchPod.setY(getViewportHeight()-320);

        mProjectileSprite = Assets.getProjectileRedSprite();
        mProjectileSprite.setSize(64,64);
        mProjectileSprite.setX(20);
        mProjectileSprite.setY(getViewportHeight()-520);

    }

    @Override
    public void dispose() {

    }

    @Override
    public void render(Graphics graphics,float delta) {
        mMediumFont.drawMultiLine(graphics.getBatch(),"Instructions",getHalfViewportWidth(),
            getViewportHeight()-30,
            10,BitmapFont.HAlignment.CENTER);

        mSmallFont.drawWrapped(graphics.getBatch(),
            "Stranded in space, hoping between Galaxies, you have to try and rescue as many escape pods as possible.",
            20,getViewportHeight()-80,getViewportWidth()-40);

        mSafePlanet.render(graphics);

        mSmallFont
            .drawWrapped(
                graphics.getBatch(),
                "You need to navigate you escape pod to the Safe Planet using Asteroids gravity and limited fuel before progressing to the next Galaxy.",
                120,getViewportHeight()-160,getViewportWidth()-140);

        mLaunchPod.draw(graphics.getBatch());

        mSmallFont.drawWrapped(graphics.getBatch(),"To launch the escape pod, press the launch button.",120,
            getViewportHeight()-270,getViewportWidth()-140);

        mTablet.setRotation(MathUtils.sinDeg(mRotate++)*10);
        mTablet.draw(graphics.getBatch());

        mSmallFont.drawWrapped(graphics.getBatch(),
            "Tilting will move your ship.  Tilting will also enable your escape pod thrusters, but be careful as "+
                "the escape pod has limited fuel.",120,getViewportHeight()-350,getViewportWidth()-140);

        mProjectileSprite.draw(graphics.getBatch());

        mSmallFont.drawWrapped(graphics.getBatch(),
            "Landing your escape pod without using fuel will result in extra pods.",120,getViewportHeight()-460,
            getViewportWidth()-140);

        
        mAsteroid.update(delta);
        mAsteroid.render(graphics);
        
        mSmallFont.drawWrapped(graphics.getBatch(),
            "Asteroids have gravity, which will influence the trajectory of the escape pod.  If the escape pod hits an asteroid, the escape pod" +
            " will be damaged.  Asteroids can be destroyed.",120,getViewportHeight()-540,
            getViewportWidth()-140);
        
        graphics.end();
    }

    @Override
    public void create() {

    }

    @Override
    public void resize(int width,int height) {
        super.resize(width,height);
        mSafePlanet = new SafePlanet();
        mSafePlanet.init();
        Vector2 location = new Vector2(20,getViewportHeight()-220);
        mSafePlanet.calculateImageShape(30,location);
        
        mAsteroid = new Asteroid();
        mAsteroid.init();
        mAsteroid.setLocation(40,getViewportHeight()-600);
    }

    @Override
    public boolean tap(float x,float y,int count,int button) {
        getScreenManager().showMenuScreen();
        return true;
    }

}
