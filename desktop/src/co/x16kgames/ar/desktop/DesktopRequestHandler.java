package co.x16kgames.ar.desktop;

import co.x16kgames.common.PlatformRequestHandler;
import co.x16kgames.common.Utils;

/**
 * Dummy Class to handle adverts.
 *
 * @author dhudson - created 15 Oct 2014
 * @since 1.1
 */
public class DesktopRequestHandler implements PlatformRequestHandler {

    @Override
    public void showBannerAd(boolean show) {
        Utils.debug("Show Ad "+show);

    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void loadAds() {
    }

    @Override
    public void showInterstitial() {
        // TODO Auto-generated method stub

    }

    @Override
    public String getVersion() {
        return "1.1";
    }
}
