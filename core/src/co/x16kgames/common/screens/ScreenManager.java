package co.x16kgames.common.screens;

import co.x16kgames.ar.screens.CreditsScreen;
import co.x16kgames.ar.screens.GameOverScreen;
import co.x16kgames.ar.screens.GameScreen;
import co.x16kgames.ar.screens.InstructionsScreen;
import co.x16kgames.ar.screens.MainMenuScreen;
import co.x16kgames.ar.screens.SplashScreen;
import co.x16kgames.common.AbstractGame;

import com.badlogic.gdx.utils.Disposable;

/**
 * Handles movement between screens.
 *
 * @author dhudson - 10-Sep-2014
 * @since 1.0
 */
public class ScreenManager implements Disposable {

    private final AbstractGame mGame;
    private GameScreen mGameScreen;
    private MainMenuScreen mMenuScreen;
    private GameOverScreen mGameOverScreen;
    private CreditsScreen mCreditsScreen;
    private SplashScreen mSplashScreen;
    private InstructionsScreen mInstructionsScreen;
    
    public ScreenManager(AbstractGame game) {
        mGame = game;
    }

    public void showSplashScreen() {
        if (mSplashScreen==null) {
            mSplashScreen = new SplashScreen(mGame);
            mSplashScreen.create();
        }

        setScreen(mSplashScreen);
    }

    public void showGameScreen() {
        if (mGameScreen==null) {
            mGameScreen = new GameScreen(mGame);
            mGameScreen.create();
        }

        // Turn off the ad during game play.
        mGame.getRequestHandler().showBannerAd(false);
        setScreen(mGameScreen);
    }

    public void showMenuScreen() {
        if (mMenuScreen==null) {
            mMenuScreen = new MainMenuScreen(mGame);
            mMenuScreen.create();
        }

        // On the main menu screen, show the add.
        mGame.getRequestHandler().showBannerAd(true);

        setScreen(mMenuScreen);
    }

    public void showGameOverScreen() {
        if (mGameOverScreen==null) {
            mGameOverScreen = new GameOverScreen(mGame);
            mGameOverScreen.create();
        }

        mGame.getRequestHandler().showInterstitial();

        setScreen(mGameOverScreen);
    }

    public void showCreditsScreen() {
        if (mCreditsScreen==null) {
            mCreditsScreen = new CreditsScreen(mGame);
            mCreditsScreen.create();
        }

        setScreen(mCreditsScreen);
    }

    public void showInstructionScreen() {
        if(mInstructionsScreen == null) {
            mInstructionsScreen = new InstructionsScreen(mGame);
            mInstructionsScreen.create();
        }
        
        setScreen(mInstructionsScreen);
    }
    
    private void setScreen(AbstractGameScreen screen) {
        screen.init();
        mGame.setScreen(screen);
    }

    @Override
    public void dispose() {
        if (mGameScreen!=null) {
            mGameScreen.dispose();
        }

        if (mMenuScreen!=null) {
            mMenuScreen.dispose();
        }

        if (mGameOverScreen!=null) {
            mGameOverScreen.dispose();
        }

        if (mCreditsScreen!=null) {
            mCreditsScreen.dispose();
        }

        if (mSplashScreen!=null) {
            mSplashScreen.dispose();
        }
    }
}
