package co.x16kgames.common.panels;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import co.x16kgames.common.GameStatus;
import co.x16kgames.common.planets.Body;

/**
 * Display crashed planet info.
 *
 * @author dhudson - 16-Sep-2014
 * @since 1.1
 */
public class CrashedWithPlanet extends AbstractPanel {

    private final Label mBodyType;
    private final Label mBodyName;

    public CrashedWithPlanet(Camera camera) {
        super(camera);
        mRoot.add(createLabel("Gravity Sucks!")).colspan(3).pad(10).align(Align.center).row();
        mBodyType = createLabel();
        mRoot.add(mBodyType).align(Align.left).padLeft(10).padBottom(10).padRight(4);
        mBodyName = createLabel();
        mRoot.add(mBodyName).align(Align.left).padRight(10).padBottom(10);
    }

    @Override
    public void populate() {
        Body body = GameStatus.getCrashedInto();

        mBodyName.setText(body.getName());
        mBodyType.setText(body.getType());

        packAndCentre();
    }
}
