package co.x16kgames.common;


/**
 * This interface separates out the platform dependent code from code.
 *
 * Each platform should should have its own implementation of this interface.
 *
 * @author dhudson - created 15 Oct 2014
 * @since 1.1
 */
public interface PlatformRequestHandler {

    public void showBannerAd(boolean show);

    public void showInterstitial();

    public void onDestroy();

    public void onResume();

    public void onPause();
    
    public void loadAds();
    
    public String getVersion();
    // public void takeScreenShot
}
