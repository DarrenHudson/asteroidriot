package co.x16kgames.ar.ios;

import org.robovm.apple.coregraphics.CGRect;
import org.robovm.apple.coregraphics.CGSize;
import org.robovm.apple.foundation.NSBundle;
import org.robovm.apple.foundation.NSDictionary;
import org.robovm.apple.foundation.NSString;
import org.robovm.apple.uikit.UIViewController;
import org.robovm.apple.uikit.UIWindow;
import org.robovm.bindings.admob.GADAdSize;
import org.robovm.bindings.admob.GADBannerView;
import org.robovm.bindings.admob.GADBannerViewDelegateAdapter;
import org.robovm.bindings.admob.GADInterstitial;
import org.robovm.bindings.admob.GADInterstitialDelegateAdapter;
import org.robovm.bindings.admob.GADRequest;
import org.robovm.bindings.admob.GADRequestError;

import co.x16kgames.ar.AsteroidRiot;
import co.x16kgames.common.PlatformRequestHandler;
import co.x16kgames.common.Utils;

import com.badlogic.gdx.backends.iosrobovm.IOSApplication;
import com.badlogic.gdx.backends.iosrobovm.UIScreen;

/**
 * IOS Platform handler.
 *
 * Get the bindings from https://github.com/BlueRiverInteractive/robovm-ios-bindings.
 * http://libgdx.badlogicgames.com/robovm-ios-bindings/
 *
 * @author dhudson - 27-Oct-2014
 * @since 3.
 */
public class IOSRequestHandler implements PlatformRequestHandler {

    public static final String HOME_SCREEN_ID = "ca-app-pub-6142084001875032/4482356100";
    public static final String GAME_OVER_ID = "ca-app-pub-6142084001875032/5959089306";

    private static final boolean USE_TEST_DEVICES = AsteroidRiot.isDebugging;

    private GADBannerView mAdview;
    private IOSApplication mIOSApplication;
    private float mBannerHeight;

    private CGRect mVisibleRect;
    private CGRect mInvisibleRect;

    private boolean isBannerLoaded;

    private GADInterstitial mInterstitial;
    private UIWindow mWindow;

    private UIViewController mRootViewController;
    private boolean isShowing = false;

    public IOSRequestHandler() {
        mBannerHeight = 0;
    }

    void setIOSApplication(IOSApplication iosApplication) {
        mIOSApplication = iosApplication;
    }

    void createAds() {
        createBannerAd();
        createInterstitialAd();
    }

    private void createBannerAd() {

        mAdview = new GADBannerView(GADAdSize.smartBannerPortrait());
        mAdview.setAdUnitID(HOME_SCREEN_ID);
        mAdview.setRootViewController(mIOSApplication.getUIViewController());

        mAdview.setDelegate(new GADBannerViewDelegateAdapter() {
            @Override
            public void didReceiveAd(GADBannerView view) {
                super.didReceiveAd(view);

                Utils.debug("ios:DidReceive ...");

                // Only do this once ..
                if (!isBannerLoaded) {
                    isBannerLoaded = true;
                    mIOSApplication.getUIViewController().getView().addSubview(mAdview);
                    
                    final CGSize screenSize = UIScreen.getMainScreen().getBounds().size();
                    final CGSize adSize = mAdview.getBounds().size();

                    // Keep aspect ratio.
                    float bannerWidth = (float)screenSize.width();
                    mBannerHeight = (float)(bannerWidth/adSize.width()*adSize.height());
                    // Position at the bottom of the screen.
                    mVisibleRect =
                        new CGRect((screenSize.width()/2)-adSize.width()/2,screenSize.height()-adSize.height(),
                            bannerWidth,mBannerHeight);
                    // mVisibleRect = new CGRect((screenSize.width()/2)-adSize.width()/2,0,bannerWidth,mBannerHeight);
                    mInvisibleRect = new CGRect(0,-mBannerHeight,bannerWidth,mBannerHeight);

                    show();
                }
            }

            @Override
            public void didFailToReceiveAd(GADBannerView view,
            GADRequestError error) {
                super.didFailToReceiveAd(view,error);
                isBannerLoaded = false;
            }
        });
    }

    private void createInterstitialAd() {

        mInterstitial = new GADInterstitial();
        mInterstitial.setAdUnitID(GAME_OVER_ID);

        mInterstitial.setDelegate(new GADInterstitialDelegateAdapter() {

            @Override
            public void didFailToReceiveAd(GADInterstitial ad,GADRequestError error) {
                super.didFailToReceiveAd(ad,error);
                Utils.debug("Interstitial failed to load ["+error.description()+"] "+error.getErrorCode());
            }

            @Override
            public void didDismissScreen(GADInterstitial ad) {
                super.didDismissScreen(ad);

                mWindow.setHidden(true);
                createInterstitialAd();
                loadInterstitial();
            }
        });
    }

    @Override
    public void showBannerAd(boolean show) {
        isShowing = show;
        if (show) {
            if (isBannerLoaded) {
                show();
            }
        }
        else {
            if (isBannerLoaded) {
                hide();
            }
        }
    }

    @Override
    public void showInterstitial() {
        if (mRootViewController==null) {
            mRootViewController = new UIViewController();
        }
        if (mWindow==null) {
            mWindow = new UIWindow(UIScreen.getMainScreen().getBounds());
            mWindow.setRootViewController(mRootViewController);
        }

        mWindow.makeKeyAndVisible();
        mInterstitial.present(mRootViewController);
    }

    @Override
    public void loadAds() {
        mAdview.loadRequest(GADRequest.create());
        loadInterstitial();
    }

    private void loadInterstitial() {
        mInterstitial.loadRequest(GADRequest.create());
    }

    public void hide() {
        // Should never happen, but better to be safe than sorry.
        if (mInvisibleRect!=null) {
            mAdview.setFrame(mInvisibleRect);
            mAdview.setHidden(true);
        }
    }

    public void show() {
        // Only show the ad, if it has been loaded and it is required.
        if (mVisibleRect!=null&&isShowing) {
            mAdview.setHidden(false);
            mAdview.setFrame(mVisibleRect);
        }
    }

    @Override
    public void onDestroy() {
        mAdview.dispose();
        mInterstitial.dispose();
        mWindow.dispose();
    }

    @Override
    public void onResume() {
        show();
    }

    @Override
    public void onPause() {
        hide();
    }

    @Override
    public String getVersion() {
        NSDictionary infoDictionary = NSBundle.getMainBundle().getInfoDictionary();
        return infoDictionary.get(new NSString("CFBundleShortVersionString")).toString();
    }

}
