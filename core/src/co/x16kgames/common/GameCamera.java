package co.x16kgames.common;

import co.x16kgames.common.projectile.Projectile;
import co.x16kgames.ar.screens.GameScreen;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.viewport.Viewport;

/**
 * Camera class which has lots of extra features to handle the Galaxy.
 *
 * If anything modifies zoom, then the bounding box must be re-calculated.
 *
 * @author dhudson - 09-Sep-2014
 * @since 1.0
 */
public class GameCamera extends OrthographicCamera {

    // This sets GL Viewport..
    private Viewport mViewport;

    // Used in pan to make sure that we do not overstep bounds
    private final Vector3 mLastPosition = new Vector3();

    // Bounding box min and max for panning
    private final Rectangle mBoundingBox;

    private Platform mPlatform;
    private Projectile mProjectile;

    private final Vector2 mTmpVector;
    private final GameScreen mGameScreen;

    public GameCamera(GameScreen gameScreen) {
        super(480,800);
        mGameScreen = gameScreen;
        mBoundingBox = new Rectangle();
        mTmpVector = new Vector2();
    }

    /**
     * This should be called after the Platform init..
     *
     * Set the camera to the centre of the projectile.
     *
     * @since 1.1
     */
    public void init() {
        mPlatform = mGameScreen.mPlatform;
        mBoundingBox.x = mGameScreen.getHalfViewportWidth();
        mBoundingBox.y = mGameScreen.getHalfViewportHeight();
        mProjectile = mPlatform.getProjectile();
        mViewport = mGameScreen.getViewport();
        //setZoom(getMaxZoom());
    }

    /**
     * Update the camera.
     *
     * This takes into account the position of the projectile and will move the camera if required.
     *
     * @param delta time since last update
     * @since 1.1
     */
    public void update(float delta) {

        // if (mProjectile.isDead()) {
        // update();
        // return;
        // }
        // if (mProjectile.isLanding()) {
        // if (zoom > 1) {
        // setZoom(Math.max(1, zoom - (delta * 0.2f)));
        // focusOnSafePlanet();
        // }
        // }
        update();
    }

    /**
     * Move the camera to focus back on the platform.
     *
     * @since 1.1
     */
//    public void focusOnPlatform() {
//        position.y = mBoundingBox.y;
//        position.x = mPlatform.mLocation.x+(Platform.PLATFORM_WIDTH/2);
//    }
//    private static final float FOP_SMOOTH_FACTOR = 0.2f;
//    private void focusOnSafePlanet() {
//        // Don't use pan, as its outside the box ...
//        pan(Utils.deltaLerp(position.x,Galaxy.getSafePlanet().getCentreLocation().x,FOP_SMOOTH_FACTOR),
//            Utils.deltaLerp(position.y,Galaxy.getSafePlanet().getCentreLocation().y,FOP_SMOOTH_FACTOR),false);
//    }
    /**
     * Pan to the platform.
     *
     * @since 1.1
     */
    public void panToPlatform() {
        position.y = mBoundingBox.y;
    }

    /**
     * Make sure that the platform is still on the screen.
     *
     * If not move the camera so that it is.
     *
     * @since 1.1
     */
    public void checkPlatform() {
        Vector2 projected = mViewport.project(mPlatform.getCenter(mTmpVector));

        if (projected.x < 5) {
            pan(-4, 0, true);
        }

        if (projected.x > viewportWidth - 5) {
            pan(4, 0, true);
        }
    }

    /**
     * Check to see if the platform is in view.
     *
     * @return true if the platform is in view.
     * @since 1.1
     */
    public boolean isPlatformInView() {
        return frustum.pointInFrustum(mPlatform.mLocation.x, mPlatform.mLocation.y, 0);
    }

    /**
     * Check to see if the projectile is in view.
     *
     * @return true if the projectile is in view.
     * @since 1.1
     */
    public boolean isProjectileInView() {
        return frustum.pointInFrustum(mProjectile.getLocation().x, mProjectile.getLocation().y, 0);
    }

    /**
     * Create a bounding box for panning.
     *
     * Note the width and height are the amount to pan by.
     */
    private void setBoundingBox() {
        mBoundingBox.x = (viewportWidth / 2) * zoom;
        mBoundingBox.y = (viewportHeight / 2) * zoom;
        mBoundingBox.width = (Galaxy.mWidth - viewportWidth) * zoom;
        mBoundingBox.height = (Galaxy.mHeight - viewportHeight) * zoom;
    }

    /**
     * Update the viewport.
     *
     * This will change the GL Viewport and also update the camera.
     *
     * Should be called from Screen.resize
     *
     * @param width
     * @param height
     * @since 1.1
     */
    public void updateViewport(int width, int height) {
        mViewport.update(width, height, true);
        setBoundingBox();
    }

    /**
     * Return the viewport.
     *
     * @return viewport.
     * @since 1.1
     */
    public Viewport getViewport() {
        return mViewport;
    }

    /**
     * Pan the camera.
     *
     * Using the deltas, pan the camera if allowed.
     *
     * @param deltaX
     * @param deltaY
     * @param confined true is confined to bounding box
     * @since 1.1
     */
    public void pan(float deltaX, float deltaY, boolean confined) {

        if (confined) {
            // Save
            mLastPosition.set(position);
            // Modify position with zoom;
            position.add(/*-*/deltaX * zoom, deltaY * zoom, 0);

            // Utils.debug("pan .. " + position);
            if (!mBoundingBox.contains(position.x, position.y)) {
                // if (Utils.isDebugging()) {
                // Utils.debug("Not Panning...  dx [" + deltaX + "] dy [" + deltaY + "] : x [" + position.x + "] y [" +
                // position.y + "]  bounds " + mBoundingBox.toString());
                // }
                // Restore
                position.set(mLastPosition);

                if (position.x < mBoundingBox.x) {
                    position.x = mBoundingBox.x;
                }

            }
        } else {
            // Modify position with zoom;
            position.add(/*-*/deltaX * zoom, deltaY * zoom, 0);
        }

        update();
    }

    /**
     * Zoom the camera.
     *
     * @param initialDistance
     * @param distance
     * @since 1.1
     */
    public void zoom(float initialDistance, float distance) {
        float ratio = initialDistance / distance;
        setZoom(MathUtils.clamp(1.0f * ratio, 0.1f, getMaxZoom()));
    }

    public void zoomToFit() {
        setZoom(getMaxZoom());
    }

    private float getMaxZoom() {
        if (Galaxy.mHeight > viewportHeight) {
            return viewportHeight / Galaxy.mHeight;
        }

        return Galaxy.mHeight / viewportHeight;
    }

    private void setZoom(float value) {
        zoom = value;
        Utils.debug("Setting zoom to " + zoom);
        update();
        setBoundingBox();
    }
}
