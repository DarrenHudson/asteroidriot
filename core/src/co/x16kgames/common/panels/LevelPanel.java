package co.x16kgames.common.panels;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import co.x16kgames.common.Galaxy;
import co.x16kgames.common.GameStatus;
import static co.x16kgames.common.Platform.PLATFORM_WIDTH;
import co.x16kgames.common.Utils;

/**
 * Display the galaxy level panel.
 *
 * Galaxy Name and Distance to safe planet.
 *
 * @author dhudson - 16-Sep-2014
 * @since 1.1
 */
public class LevelPanel extends AbstractPanel {

    private final Label mGalaxyName;
    private final Label mLevel;

    public LevelPanel(Camera camera) {
        super(camera);

        mRoot.columnDefaults(0).left().padLeft(10).padRight(5);
        mRoot.columnDefaults(1).right().padRight(10);

        mRoot.add("Galaxy").padTop(10);

        mGalaxyName = createLabel();
        mRoot.add(mGalaxyName).padTop(10).row();

        mRoot.add("Level").padBottom(10);
        mLevel = createLabel();
        mRoot.add(mLevel).padBottom(10);
    }

    @Override
    public void populate() {
        mGalaxyName.setText(Galaxy.getName());
        mLevel.setText(Integer.toString(GameStatus.mLevel));

        packAndCentre();
    }

}
