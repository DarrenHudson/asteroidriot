package co.x16kgames.common.lw;

import co.x16kgames.common.Graphics;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.math.Vector2;

/**
 * An array of buttons, not to be mistaken for AWT button group.
 *
 * @author dhudson - created 22 Sep 2014
 * @since 1.1
 */
public class LWButtonGroup {

    private List<LWButton> mList;

    public LWButtonGroup() {
        mList = new ArrayList<LWButton>(5);
    }

    public void add(LWButton button) {
        mList.add(button);
    }

    /**
     * Perform render on all buttons in the group.
     * 
     * @param graphics
     * @since 1.1
     */
    public void render(Graphics graphics) {
        for (LWButton button:mList) {
            button.render(graphics);
        }
    }

    /**
     * Perform update on all of the buttons in the group.
     * 
     * @param delta time
     * @since 1.1
     */
    public void update(float delta) {
        for (LWButton button:mList) {
            button.update(delta);
        }
    }

    /**
     * Check to see if the position (Unprojected) is one of the buttons.
     * 
     * If it is one of the buttons, the run the ButtonListener.tap
     * 
     * @param location (Unprojected)
     * @return true if the tap position was in the group
     * @since 1.1
     */
    public boolean tap(Vector2 location) {
        for (LWButton button:mList) {
            if (button.contains(location)) {
                button.notifyListener(location);
                return true;
            }
        }

        return false;
    }
}
