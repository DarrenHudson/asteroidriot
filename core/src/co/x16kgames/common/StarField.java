package co.x16kgames.common;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import co.x16kgames.common.planets.Star;
import co.x16kgames.common.projectile.Projectile;

/**
 * A Star Field can have between MIN and MAX stars.
 *
 * Instead of creating star fields, recycle the existing star field.
 *
 * @author dhudson - 29-Aug-2014
 * @since
 */
public final class StarField implements Drawable {

    private static final int MIN_STARS = 20;
    private static final int MAX_STARS = 40;

    private final Star[] mStars = new Star[MAX_STARS];
    private int mNoOfStars;

    public StarField() {
        for (int i = 0; i < MAX_STARS; i++) {
            mStars[i] = new Star();
        }
    }

    @Override
    public void init() {
        mNoOfStars = MathUtils.random(MIN_STARS, MAX_STARS);
        for (int i = 0; i < mNoOfStars; i++) {
            mStars[i].init();
        }
    }

    @Override
    public void render(Graphics graphics) {
        for (int i = 0; i < mNoOfStars; i++) {
            mStars[i].render(graphics);
        }
    }

    @Override
    public boolean intersects(Rectangle rect) {
        return false;
    }

    @Override
    public void update(float delta) {
        for (int i = 0; i < mNoOfStars; i++) {
            mStars[i].update(delta);
        }
    }

    @Override
    public void interact(Projectile projectile) {
    }

    @Override
    public boolean isDead() {
        return false;
    }

    @Override
    public void setLocation(float x, float y) {
    }

    @Override
    public Rectangle getBounds() {
        return Utils.EMPTY_RECT;
    }

}
