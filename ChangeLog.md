Changes for version 2.  24-10-2014
Lighter font.
More fuel.
Less asteroids.
Credits screen.

Changes for version 5. 03-11-2014

Core.
Make the launch ship and escape pod bigger.
Make game play easier.
Earth now reduces in size and mass each level progressed.
Added version to credits screen.

iOS changes.
Fixed bug for banner ads.
Fixed bug that adverts were not paused.
Fixed bug that screen size changed when banner ads were displayed.

Android.
Fixed bug that it was possible to display a banner ad during play.
Fixed bug that squashed the screen. 

Changes for version 5.2 15-11-2014

Core.
Changed spelling mistake in game over screen.
Asteroids reappear after being destroyed.
Fixed bug that some bodies were not drawn within the federation.
Changed launch button
New credits screen

Changes for version 5.3 27-12-2014

Core.
Instructions screen.
Landing an escape pod without the use of fuel will now result in an extra escape pod.
