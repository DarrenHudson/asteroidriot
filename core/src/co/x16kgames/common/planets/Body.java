package co.x16kgames.common.planets;

import co.x16kgames.ar.Assets;
import co.x16kgames.common.Angle;
import co.x16kgames.common.Drawable;
import co.x16kgames.common.GameStatus;
import co.x16kgames.common.Graphics;
import co.x16kgames.common.Utils;
import co.x16kgames.common.projectile.Projectile;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * Abstract Class to represent a body in the universe.
 *
 * The circle x and y is the location, which is the centre of the circle
 *
 * @author dhudson - created 17 Feb 2014
 * @since 1.0
 */
public abstract class Body extends Circle implements Drawable {

    private static final long serialVersionUID = 1L;

    private String mName;
    private float mMass;

    private final Vector2 mCentreLocation;

    private final Sprite mSprite;

    // Used for calculation etc, don't want to create new ones all of the time.
    public final Vector2 mTmpVector = new Vector2();
    public final Rectangle mTmpRect = new Rectangle();

    /**
     * Universal gravitational constant
     */
    public static final float G = (float)6.6742e-11;

    /**
     * Constructor.
     *
     * @since 1.0
     */
    public Body(Sprite sprite) {
        mSprite = sprite;
        mCentreLocation = new Vector2();
    }

    /**
     * Configure the sprite for size and origin.
     *
     * radius must be set first.
     */
    public void configureSprite() {
        mSprite.setSize(radius*2,radius*2);
        mSprite.setOriginCenter();
    }

    public void setBodyName() {
        mName = Assets.getRandomPlanetName();
    }

    /**
     * Set the location in the universe of the object
     *
     * @param location of the body
     * @since 1.0
     */
    public void setLocation(Vector2 location) {
        setLocation(location.x,location.y);
    }

    @Override
    public void setLocation(float x,float y) {
        this.x = x;
        this.y = y;
    }

    public Vector2 getCentreLocation() {
        mCentreLocation.x = x+radius;
        mCentreLocation.y = y+radius;
        return mCentreLocation;
    }

    /**
     * Get bounds
     *
     * @return return the graphical bounds of the body, including orbiting bodies.
     * @since 1.0
     */
    @Override
    public Rectangle getBounds() {
        mTmpRect.x = x;
        mTmpRect.y = y;
        mTmpRect.width = mSprite.getWidth();
        mTmpRect.height = mSprite.getHeight();
        return mTmpRect;
    }

    /**
     * Return the mass of the body.
     *
     * @return the mass of the body.
     * @since 1.0
     */
    public float getMass() {
        return mMass;
    }

    /**
     * Set the mass of the body
     *
     * @param mass of the body
     * @since 1.0
     */
    public void setMass(float mass) {
        mMass = mass;
    }

    /**
     * Return the name of the planet
     *
     * @return the name of the body
     * @since 1.0
     */
    public String getName() {
        return mName;
    }

    /**
     * Draw the sprite at the circle x,y.
     *
     * @param graphics
     * @since 1.1
     */
    public void drawSprite(Graphics graphics) {
        mSprite.setX(x);
        mSprite.setY(y);
        mSprite.draw(graphics.getBatch());
    }

    @Override
    public boolean intersects(Rectangle rect) {
        return rect.overlaps(getBounds());
    }

    /**
     * Check to see if one circle overlaps another.
     *
     * @param circle
     * @return true if the circles overlap.
     */
    public boolean intersects(Circle circle) {
        return circle.overlaps(circle);
    }

    @Override
    public String toString() {
        return getName()+" : "+getType()+" : "+getMass();
    }

    @Override
    public void interact(Projectile projectile) {

        if (projectile.isDead()) {
            return;
        }

        // From the centre of the planet to the projectile
        float distance = projectile.getCentreLocation().dst(getCentreLocation());
        // minus the radius of the planet ...
        distance -= (radius);

        // Projectile has hit the planet
        if (distance<1) {
            projectileHit(projectile);
            return;
        }

        calculateGravityEffect(distance,projectile);
    }

    public Sprite getSprite() {
        return mSprite;
    }

    public void projectileHit(Projectile projectile) {
        GameStatus.setBodyCrashedInto(this);
        projectile.setDead(true);
    }

    /**
     * Return the angle of the point to the centre of the body.
     *
     * @param point
     * @return angle in degrees
     */
    public float calculateAngle(Vector2 point) {
        return mTmpVector.set(point).sub(getCentreLocation()).angle();
    }

    public float calculateAngleOfImpact(Vector2 point) {
        float angle = MathUtils.atan2(y,x)
            -MathUtils.atan2(point.y,point.x);
        return Angle.toDegrees(angle);
    }

    public float calculateAngleOfReflection(Vector2 point) {
        float angle = MathUtils.atan2(y,x)
            -MathUtils.atan2(point.y,point.x);

        return Angle.toDegrees(MathUtils.PI+angle);
    }

    /**
     * Calculate the gravitational effect of the the body.
     *
     * g = (GM) / ( r^2 ) G = Universal Gravitational Constant M = Mass of the body r = Distance from the centre of the
     * body.
     *
     * @param distance from the centre of the body
     * @return the gravitational effect of the body
     * @since 1.0
     */
    public float calculateGravity(float distance) {
        // double g = (G * theMass) / (Math.pow(distance,2));
        return MathUtils.clamp((mMass/(distance*distance)),0,1);
    }

    /**
     * This is the basic LERP calculation. pu = p0 + (p1 - p0) * u Where p0 = Projectile centre location p1 = Centre of
     * Body u = The progress. It is given in percentage, between 0 and 1. Which in our case is the Gravity Effect.
     *
     * @param distance
     * @param projectile
     */
    public void calculateGravityEffect(float distance,Projectile projectile) {

        if (projectile.isInFlight()) {
            // Faster the projectile, the less gravitational effect
            float gravityEffect = calculateGravity(distance);

            if (gravityEffect<0.009) {
                // Its too far away
                return;
            }

            float deltaX = MathUtils.lerp(projectile.getCentreLocation().x,getCentreLocation().x,gravityEffect);
            projectile.addXVelocity(deltaX-projectile.getCentreLocation().x);

            // Calculate Y
            float deltaY = MathUtils.lerp(projectile.getCentreLocation().y,getCentreLocation().y,gravityEffect);
            projectile.addYVelocity(deltaY-projectile.getCentreLocation().y);

        }
    }

    public void changeLocation(Rectangle bounds) {
        float size = radius*2;
        mTmpRect.x = bounds.x + size;
        mTmpRect.y = bounds.y + size;
        mTmpRect.width = bounds.width-(size*2);
        mTmpRect.height = bounds.height-(size*2);

        setLocation(Utils.getRandomPoint(mTmpRect));
    }

    @Override
    public boolean isDead() {
        return false;
    }

    @Override
    public abstract void init();

    /**
     * Check to see if body is a planet
     * 
     * @return true if a planet
     * @since 1.1
     */
    public boolean isPlanet() {
        return false;
    }

    /**
     * Return the type of body
     *
     * @return the type of body
     * @since 1.0
     */
    public abstract String getType();
}
